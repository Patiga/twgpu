use crate::shared::ScissorRect;
use crate::Camera;
use vek::Vec2;
use wgpu::RenderPass;

/// Wrapper type around [`wgpu::RenderPass`] to pass around important context information
pub struct TwRenderPass<'pass> {
    pub render_pass: RenderPass<'pass>,
    /// Size of the render target
    pub size: Vec2<u32>,
    pub scissor_rect: ScissorRect,
    pub camera: Camera,
}

impl<'pass> TwRenderPass<'pass> {
    pub fn new(render_pass: RenderPass<'pass>, size: Vec2<u32>, camera: &Camera) -> Self {
        TwRenderPass {
            render_pass,
            size,
            scissor_rect: ScissorRect::viewport(size),
            camera: *camera,
        }
    }

    /// Returns `true` if the render area hasn't collapsed into nothingness
    #[must_use]
    pub fn intersect_scissor_rect(&mut self, other: &ScissorRect) -> bool {
        if let Some(scissor_rect) = self.scissor_rect.intersect(other) {
            self.scissor_rect = scissor_rect;
            self.scissor_rect.apply(&mut self.render_pass);
            true
        } else {
            false
        }
    }

    pub fn set_scissor_rect(&mut self, scissor_rect: &ScissorRect) {
        self.scissor_rect = *scissor_rect;
        self.scissor_rect.apply(&mut self.render_pass);
    }
}
