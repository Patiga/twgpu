use wgpu::{
    AddressMode, BindGroupEntry, BindGroupLayoutEntry, BindingResource, BindingType, Device,
    FilterMode, Sampler, SamplerBindingType, SamplerDescriptor, ShaderStages,
};

/// Stores all samplers needed by all modules.
/// Assumptions about the samplers:
///
/// - Used only in fragment shaders
/// - Filtering
pub struct Samplers {
    clamp_to_edge: Sampler,
    repeating: Sampler,
}

impl Samplers {
    pub fn bind_group_layout_entry(binding: u32) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::FRAGMENT,
            ty: BindingType::Sampler(SamplerBindingType::Filtering),
            count: None,
        }
    }

    pub fn repeating(&self, binding: u32) -> BindGroupEntry {
        BindGroupEntry {
            binding,
            resource: BindingResource::Sampler(&self.repeating),
        }
    }

    pub fn clamp_to_edge(&self, binding: u32) -> BindGroupEntry {
        BindGroupEntry {
            binding,
            resource: BindingResource::Sampler(&self.clamp_to_edge),
        }
    }

    pub fn new(device: &Device) -> Self {
        let settings = SamplerDescriptor {
            mag_filter: FilterMode::Linear,
            min_filter: FilterMode::Linear,
            mipmap_filter: FilterMode::Linear,
            ..SamplerDescriptor::default()
        };
        Self {
            clamp_to_edge: device.create_sampler(&SamplerDescriptor {
                label: Some("Clamp-To-Edge"),
                address_mode_u: AddressMode::ClampToEdge,
                address_mode_v: AddressMode::ClampToEdge,
                address_mode_w: AddressMode::ClampToEdge,
                ..settings.clone()
            }),
            repeating: device.create_sampler(&SamplerDescriptor {
                label: Some("Clamp-To-Edge"),
                address_mode_u: AddressMode::Repeat,
                address_mode_v: AddressMode::Repeat,
                address_mode_w: AddressMode::Repeat,
                ..settings.clone()
            }),
        }
    }
}
