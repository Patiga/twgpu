use crate::shared::Vertex;
use twsnap::enums;
use vek::az::UnwrappedAs;
use vek::Rgba;
use wgpu::{vertex_attr_array, VertexAttribute, VertexBufferLayout, VertexStepMode};

mod batched;
mod game_rendering;
mod particles;
mod sprites_data;
mod sprites_static;
mod tee_rendering;
mod texture_storage;

pub use batched::*;
pub use game_rendering::*;
pub use particles::*;
pub use sprites_data::*;
pub use sprites_static::*;
pub use tee_rendering::*;
pub use texture_storage::*;

#[repr(C)]
#[derive(Debug, Copy, Clone, PartialEq, bytemuck::Zeroable, bytemuck::Pod)]
pub struct SpriteVertex {
    pub vertex: Vertex,
    pub color: Rgba<f32>,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TeeSprite {
    Body,
    BodyOutline,
    EyeNormal,
    EyeAngry,
    EyePain,
    EyeHappy,
    EyeDead,
    EyeSurprise,
    Foot,
    FootOutline,
    Hand,
    HandOutline,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum GameSprite {
    Hook,
    HookTip,
    Hammer,
    Pistol,
    Shotgun,
    Grenade,
    Ninja,
    Laser,
    PistolProjectile,
    ShotgunProjectile,
    GrenadeProjectile,
    LaserProjectile,
    PistolMuzzle1,
    PistolMuzzle2,
    PistolMuzzle3,
    ShotgunMuzzle1,
    ShotgunMuzzle2,
    ShotgunMuzzle3,
    NinjaDash1,
    NinjaDash2,
    NinjaDash3,
    Heart,
    Shield,
    ShotgunShield,
    GrenadeShield,
    LaserShield,
    NinjaShield,
    BlueFlag,
    RedFlag,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ParticleSprite {
    Slice,
    Ball,
    Splat1,
    Splat2,
    Splat3,
    Smoke, // Three more smoke textures(?)
    Shell,
    Explosion,
    AirJump,
    Hit,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ExtraSprite {
    Snowflake,
}

pub type EmoticonSprite = twsnap::enums::Emoticon;

impl SpriteVertex {
    pub const ATTRIBUTES: [VertexAttribute; 2] = vertex_attr_array![0 => Float32x4, 1 => Float32x4];

    pub fn vertex_buffer_layout() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>().unwrapped_as(),
            step_mode: VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

impl From<enums::ActiveWeapon> for GameSprite {
    fn from(value: enums::ActiveWeapon) -> Self {
        match value {
            enums::ActiveWeapon::Hammer => GameSprite::Hammer,
            enums::ActiveWeapon::Pistol => GameSprite::Pistol,
            enums::ActiveWeapon::Shotgun => GameSprite::Shotgun,
            enums::ActiveWeapon::Grenade => GameSprite::Grenade,
            enums::ActiveWeapon::Rifle => GameSprite::Laser,
            enums::ActiveWeapon::Ninja => GameSprite::Ninja,
        }
    }
}

impl GameSprite {
    pub fn projectile_of(value: enums::ActiveWeapon) -> Self {
        match value {
            enums::ActiveWeapon::Hammer => GameSprite::Hammer,
            enums::ActiveWeapon::Pistol => GameSprite::PistolProjectile,
            enums::ActiveWeapon::Shotgun => GameSprite::ShotgunProjectile,
            enums::ActiveWeapon::Grenade => GameSprite::GrenadeProjectile,
            enums::ActiveWeapon::Rifle => GameSprite::LaserProjectile,
            enums::ActiveWeapon::Ninja => GameSprite::Ninja,
        }
    }
}

impl From<enums::SelectableWeapon> for GameSprite {
    fn from(value: enums::SelectableWeapon) -> Self {
        match value {
            enums::SelectableWeapon::Hammer => GameSprite::Hammer,
            enums::SelectableWeapon::Pistol => GameSprite::Pistol,
            enums::SelectableWeapon::Shotgun => GameSprite::Shotgun,
            enums::SelectableWeapon::Grenade => GameSprite::Grenade,
            enums::SelectableWeapon::Rifle => GameSprite::Laser,
        }
    }
}

impl From<enums::CollectableWeapon> for GameSprite {
    fn from(value: enums::CollectableWeapon) -> Self {
        match value {
            enums::CollectableWeapon::Shotgun => GameSprite::Shotgun,
            enums::CollectableWeapon::Grenade => GameSprite::Grenade,
            enums::CollectableWeapon::Rifle => GameSprite::Laser,
            enums::CollectableWeapon::Ninja => GameSprite::Ninja,
        }
    }
}

impl From<enums::Powerup> for GameSprite {
    fn from(value: enums::Powerup) -> Self {
        match value {
            enums::Powerup::Health => GameSprite::Heart,
            enums::Powerup::Armor => GameSprite::Shield,
            enums::Powerup::Weapon(weapon) => GameSprite::from(weapon),
            enums::Powerup::Shield(enums::CollectableWeapon::Shotgun) => GameSprite::ShotgunShield,
            enums::Powerup::Shield(enums::CollectableWeapon::Grenade) => GameSprite::GrenadeShield,
            enums::Powerup::Shield(enums::CollectableWeapon::Rifle) => GameSprite::LaserShield,
            enums::Powerup::Shield(enums::CollectableWeapon::Ninja) => GameSprite::ShotgunShield,
        }
    }
}
