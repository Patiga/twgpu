use std::ops::Range;

use crate::buffer::{GpuArrayBuffer, GpuDeviceExt, GpuVecBuffer};
use wgpu::util::RenderEncoder;
use wgpu::{
    include_wgsl, BindGroup, BlendState, BufferUsages, ColorTargetState, ColorWrites, Device,
    FragmentState, IndexFormat, MultisampleState, PipelineCompilationOptions, PrimitiveState,
    RenderPipeline, RenderPipelineDescriptor, ShaderModule, TextureFormat, VertexState,
};

use crate::sprites::texture_storage::SpriteTextures;
use crate::sprites::{SpriteVertex, TextureToken};

const LABEL: Option<&str> = Some("Chunked Sprites");

pub struct BatchSpriteRender {
    index_buffer: GpuArrayBuffer<[u32; 6]>,
    shader: ShaderModule,
    format: TextureFormat,
    pipeline: Option<RenderPipeline>,
    bind_group: Option<BindGroup>,
    current_textures: usize,
}

fn adjust_index_buffer(idx: &mut GpuArrayBuffer<[u32; 6]>, amount: usize, device: &Device) -> bool {
    if idx.len() >= amount {
        return false;
    }
    let required_size = u32::try_from(amount.next_power_of_two()).expect("Too big index buffer");
    let indices: Vec<_> = (0..required_size)
        .map(|i| [0, 1, 3, 0, 2, 3].map(|x| x + i * 4))
        .collect();
    *idx = device.array_buffer(indices.as_slice(), Some("Index "), BufferUsages::INDEX);
    true
}

impl BatchSpriteRender {
    pub fn render<'pass, R: RenderEncoder<'pass>>(
        &'pass mut self,
        sprite_buffer: &'pass GpuVecBuffer<[SpriteVertex; 4]>,
        texture_buffer: &'pass GpuVecBuffer<TextureToken>,
        textures: &'pass SpriteTextures,
        render_pass: &mut R,
        device: &Device,
    ) {
        self.render_range(
            0..sprite_buffer.len(),
            sprite_buffer,
            texture_buffer,
            textures,
            render_pass,
            device,
        )
    }

    pub fn render_range<'pass, R: RenderEncoder<'pass>>(
        &'pass mut self,
        range: Range<usize>,
        sprite_buffer: &'pass GpuVecBuffer<[SpriteVertex; 4]>,
        texture_buffer: &'pass GpuVecBuffer<TextureToken>,
        textures: &'pass SpriteTextures,
        render_pass: &mut R,
        device: &Device,
    ) {
        assert_eq!(sprite_buffer.len(), texture_buffer.len());
        if range.is_empty() {
            return;
        }
        if adjust_index_buffer(&mut self.index_buffer, sprite_buffer.len(), device)
            || textures.texture_count() != self.current_textures
        {
            let bind_group = textures.chunked_bind_group(texture_buffer, device);
            self.current_textures = textures.texture_count();
            let vertex = VertexState {
                module: &self.shader,
                entry_point: Some("vs_main"),
                compilation_options: PipelineCompilationOptions::default(),
                buffers: &[SpriteVertex::vertex_buffer_layout()],
            };
            let fragment = FragmentState {
                module: &self.shader,
                entry_point: Some("fs_main"),
                compilation_options: PipelineCompilationOptions::default(),
                targets: &[Some(ColorTargetState {
                    format: self.format,
                    blend: Some(BlendState::ALPHA_BLENDING),
                    write_mask: ColorWrites::all(),
                })],
            };
            let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: LABEL,
                bind_group_layouts: &[&textures.chunked_group_layout(device)],
                push_constant_ranges: &[],
            });
            let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
                label: LABEL,
                layout: Some(&pipeline_layout),
                vertex,
                primitive: PrimitiveState::default(),
                depth_stencil: None,
                multisample: MultisampleState::default(),
                fragment: Some(fragment),
                multiview: None,
                cache: None,
            });
            self.pipeline = Some(pipeline);
            self.bind_group = Some(bind_group);
        }
        render_pass.set_pipeline(self.pipeline.as_ref().unwrap());
        render_pass.set_vertex_buffer(0, sprite_buffer.full_slice());
        render_pass.set_index_buffer(self.index_buffer.full_slice(), IndexFormat::Uint32);
        render_pass.set_bind_group(0, self.bind_group.as_ref(), &[]);
        let count = range.end - range.start;
        render_pass.draw_indexed(0..6 * count as u32, range.start as i32 * 4, 0..1);
    }

    pub fn new(format: TextureFormat, device: &Device) -> Self {
        let shader = device.create_shader_module(include_wgsl!("batched.wgsl"));
        let index_buffer = device.array_buffer(&[], Some("Zero-sized Init"), BufferUsages::INDEX);
        Self {
            index_buffer,
            shader,
            format,
            pipeline: None,
            bind_group: None,
            current_textures: 0,
        }
    }
}
