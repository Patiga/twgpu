struct Vertex {
    @location(0) pos_uv: vec4<f32>,
    @location(1) color: vec4<f32>,
};

struct Fragment {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) color: vec4<f32>,
};

struct Camera {
    position: vec2<f32>,
    base_dimensions: vec2<f32>, // In tiles
    zoom: vec2<f32>,
    _padding: vec2<f32>,
};

@group(0)
@binding(0)
var<uniform> camera: Camera; // Vertex
@group(0)
@binding(3)
var tex: texture_2d<f32>; // Fragment
@group(0)
@binding(4)
var tex_sampler: sampler; // Fragment

// The y-axis is inverted between the map and gpu coordinate systems. Also for the camera dimensions.
const CORRECTION = vec2<f32>(2., -2.);

@vertex
fn vs_main(vertex: Vertex) -> Fragment {
    var frag: Fragment;
    var position = vertex.pos_uv.xy - camera.position;
    let factor = CORRECTION / camera.base_dimensions / camera.zoom;
    frag.clip_position = vec4<f32>(position * factor, 0., 1.);

    frag.uv = vertex.pos_uv.zw;
    frag.color = vertex.color;
    return frag;
}

@fragment
fn fs_main(frag: Fragment) -> @location(0) vec4<f32> {
    return textureSample(tex, tex_sampler, frag.uv) * frag.color;
}
