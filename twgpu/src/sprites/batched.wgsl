struct Vertex {
    @location(0) pos_uv: vec4<f32>,
    @location(1) color: vec4<f32>,
};

struct Fragment {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) color: vec4<f32>,
    @location(2) texture: u32,
};

struct Camera {
    position: vec2<f32>,
    base_dimensions: vec2<f32>, // In tiles
    zoom: vec2<f32>,
    _padding: vec2<f32>,
};

@group(0)
@binding(0)
var<uniform> camera: Camera; // Vertex
@group(0)
@binding(2)
var<storage, read> texture_indices: array<u32>; // Fragment
@group(0)
@binding(3)
var tex: binding_array<texture_2d<f32>>; // Fragment
@group(0)
@binding(4)
var tex_sampler: sampler; // Fragment

// The y-axis is inverted between the map and gpu coordinate systems. Also for the camera dimensions.
const CORRECTION = vec2<f32>(2., -2.);

@vertex
fn vs_main(@builtin(vertex_index) index: u32, vertex: Vertex) -> Fragment {
    var frag: Fragment;
    var position = vertex.pos_uv.xy - camera.position;
    let factor = CORRECTION / camera.base_dimensions / camera.zoom;
    frag.clip_position = vec4<f32>(position * factor, 0., 1.);

    frag.uv = vertex.pos_uv.zw;
    frag.color = vertex.color;
    frag.texture = texture_indices[index / u32(4)];
    return frag;
}

@fragment
fn fs_main(frag: Fragment) -> @location(0) vec4<f32> {
    //return vec4<f32>(f32(frag.texture) / 255, 0., 0., 1);
    return textureSample(tex[frag.texture], tex_sampler, frag.uv) * frag.color;
}
