use twmap::TwMap;
use wgpu::Device;

use super::{GpuGroupRender, GpuMapDataDyn, GpuMapDataStatic, GpuMapStatic};
use crate::textures::Samplers;
use crate::{GpuCamera, TwRenderPass};

pub struct GpuMapRender {
    pub groups: Vec<GpuGroupRender>,
    pub game_group: usize,
}

impl GpuMapStatic {
    pub fn prepare_render(
        &self,
        map: &TwMap,
        data_static: &GpuMapDataStatic,
        data_dyn: &GpuMapDataDyn,
        camera: &GpuCamera,
        samplers: &Samplers,
        device: &Device,
    ) -> GpuMapRender {
        let groups: Vec<GpuGroupRender> = map
            .groups
            .iter()
            .zip(data_static.groups.iter())
            .zip(data_dyn.tilemap_vertices.iter())
            .map(|((group, group_data), tilemap_vertices)| {
                self.prepare_group_render(
                    group,
                    group_data,
                    data_static,
                    data_dyn,
                    camera,
                    tilemap_vertices.as_slice(),
                    samplers,
                    device,
                )
            })
            .collect();
        let game_group = groups.iter().position(|g| g.game_layer_cutoff.is_some());
        GpuMapRender {
            groups,
            game_group: game_group.unwrap_or(0),
        }
    }
}

impl GpuMapRender {
    pub fn render_foreground<'pass>(&'pass self, render_pass: &mut TwRenderPass<'pass>) {
        self.render(true, render_pass);
    }

    pub fn render_background<'pass>(&'pass self, render_pass: &mut TwRenderPass<'pass>) {
        self.render(false, render_pass);
    }

    /// Render size parameter passes the width and height of the render target in pixels
    fn render<'pass>(&'pass self, foreground: bool, render_pass: &mut TwRenderPass<'pass>) {
        let scissor_rect = render_pass.scissor_rect;
        let groups = match foreground {
            false => &self.groups[..self.game_group + 1],
            true => &self.groups[self.game_group..],
        };
        for group in groups {
            render_pass.set_scissor_rect(&scissor_rect);
            group.render(foreground, render_pass);
        }
        render_pass.set_scissor_rect(&scissor_rect);
    }
}
