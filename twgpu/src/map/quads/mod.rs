use std::mem;
use vek::az::UnwrappedAs;
use vek::{Rgba, Uv, Vec2};
use wgpu::{vertex_attr_array, VertexAttribute, VertexBufferLayout, VertexStepMode};

mod quads_data;
mod quads_render;
mod quads_static;
pub use quads_data::GpuQuadsData;
pub use quads_render::GpuQuadsRender;
pub use quads_static::GpuQuadsStatic;

/// Vertex struct for the quads
/// `center` and the `env_indices` are the same for all 4 corners of the quad
#[derive(Debug, Copy, Clone, bytemuck::Zeroable, bytemuck::Pod)]
#[repr(C)]
pub struct QuadCorner {
    /// First two coordinates are the center of the quad, next two the offset of the quad corner
    pub center: Vec2<f32>,
    pub offset: Vec2<f32>,
    pub tex_coords: Uv<f32>,
    pub color: Rgba<f32>,
    pub color_env: i32,
    pub position_env: i32,
}

impl QuadCorner {
    pub const ATTRIBUTES: [VertexAttribute; 4] =
        vertex_attr_array![0 => Float32x4, 1 => Float32x2, 2 => Float32x4, 3 => Sint32x2];

    pub fn vertex_buffer_layout() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: mem::size_of::<Self>().unwrapped_as(),
            step_mode: VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }
}
