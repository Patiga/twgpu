use wgpu::{
    include_wgsl, BindGroupLayout, BindGroupLayoutDescriptor, BlendState, ColorTargetState,
    ColorWrites, Device, FragmentState, PipelineCompilationOptions, PipelineLayoutDescriptor,
    PrimitiveState, PrimitiveTopology, RenderPipeline, RenderPipelineDescriptor, TextureFormat,
    TextureViewDimension, VertexState,
};

use super::super::{GpuEnvelopesData, QuadCorner};
use crate::map::group_data::GroupInfoBuffer;
use crate::textures::{Mapres, Samplers};
use crate::GpuCamera;

const LABEL: Option<&str> = Some("Quads Static");

pub struct GpuQuadsStatic {
    pub format: TextureFormat,
    pub bind_group_layout: BindGroupLayout,
    pub pipeline: RenderPipeline,
}

impl GpuQuadsStatic {
    pub fn new(format: TextureFormat, device: &Device) -> Self {
        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: LABEL,
            entries: &[
                GpuCamera::bind_group_layout_entry(0),
                GpuEnvelopesData::layout_entry(1),
                GroupInfoBuffer::bind_group_layout_entry(2),
                Mapres::texture_layout_entry(3, TextureViewDimension::D2),
                Samplers::bind_group_layout_entry(4),
            ],
        });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: LABEL,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });
        let shader_module = device.create_shader_module(include_wgsl!("quad_shader.wgsl"));
        let vertex_state = VertexState {
            module: &shader_module,
            entry_point: Some("vs_main"),
            compilation_options: PipelineCompilationOptions::default(),
            buffers: &[QuadCorner::vertex_buffer_layout()],
        };
        let fragment_state = FragmentState {
            module: &shader_module,
            entry_point: Some("fs_main"),
            compilation_options: PipelineCompilationOptions::default(),
            targets: &[Some(ColorTargetState {
                format,
                blend: Some(BlendState::ALPHA_BLENDING),
                write_mask: ColorWrites::all(),
            })],
        };
        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: LABEL,
            layout: Some(&pipeline_layout),
            vertex: vertex_state,
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                ..PrimitiveState::default()
            },
            depth_stencil: None,
            multisample: Default::default(),
            fragment: Some(fragment_state),
            multiview: None,
            cache: None,
        });
        Self {
            format,
            bind_group_layout,
            pipeline,
        }
    }
}
