use wgpu::{Device, TextureFormat};

use super::{GpuQuadsStatic, GpuTilemapStatic};

pub struct GpuMapStatic {
    pub format: TextureFormat,
    pub tilemap: GpuTilemapStatic,
    pub quads: GpuQuadsStatic,
}

impl GpuMapStatic {
    pub fn new(format: TextureFormat, device: &Device) -> Self {
        GpuMapStatic {
            format,
            tilemap: GpuTilemapStatic::new(format, device),
            quads: GpuQuadsStatic::new(format, device),
        }
    }
}
