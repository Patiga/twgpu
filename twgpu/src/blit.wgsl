@group(0) @binding(0)
var tex: texture_2d<f32>;
@group(0) @binding(1)
var tex_sampler: sampler;

struct VertexFragment {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) uv: vec2<f32>,
}

// We build 4 vertices, one for each corner.
// The order is: 0: top-left, 1: top-right, 2: bottom-left, 3: bottom-right
// `position` is (1, 1) for top-right, (-1, -1) for bottom-left
// `uv` is (0, 0) for top-left, (1, 1) for bottom-right

const UVS = array<vec2<f32>, 4>(
    vec2<f32>(0., 0.),
    vec2<f32>(1., 0.),
    vec2<f32>(0., 1.),
    vec2<f32>(1., 1.),
);

const POSITIONS= array<vec4<f32>, 4>(
    vec4<f32>(-1.,  1., 0., 1.),
    vec4<f32>( 1.,  1., 0., 1.),
    vec4<f32>(-1., -1., 0., 1.),
    vec4<f32>( 1., -1., 0., 1.),
);

@vertex
fn vs_main(
    @builtin(vertex_index) index: u32
) -> VertexFragment {
    var out: VertexFragment;
    var positions = POSITIONS;
    var uvs = UVS;
    out.clip_position = positions[index];
    out.uv = uvs[index];
    return out;
}

@fragment
fn fs_main(in: VertexFragment) -> @location(0) vec4<f32> {
    return textureSample(tex, tex_sampler, in.uv);
}
