//! This crate provides a [middleware](https://github.com/gfx-rs/wgpu/wiki/Encapsulating-Graphics-Work#middleware-libraries) library for [Teeworlds](https://www.teeworlds.com/) and [DDNet](https://ddnet.org/) rendering.

pub mod blit;
pub mod buffer;
mod camera;
/// Map-specific rendering code
pub mod map;
mod mipmap;
/// Structs and traits that are helpful in multiple modules
pub mod shared;
//pub mod tee;
pub mod sprites;
pub mod textures;
mod tw_render_pass;

pub use camera::{Camera, GpuCamera};
pub use tw_render_pass::TwRenderPass;

const LABEL: Option<&str> = Some("TwGpu");

pub fn device_descriptor_for_batched_sprites(adapter: &wgpu::Adapter) -> wgpu::DeviceDescriptor {
    wgpu::DeviceDescriptor {
        label: LABEL,
        required_features: BATCHED_SPRITES_FEATURES,
        required_limits: limits_for_batched_sprites(adapter),
        memory_hints: wgpu::MemoryHints::Performance,
    }
}

pub fn device_descriptor(adapter: &wgpu::Adapter) -> wgpu::DeviceDescriptor {
    wgpu::DeviceDescriptor {
        label: LABEL,
        required_features: FEATURES,
        required_limits: limits(adapter),
        memory_hints: wgpu::MemoryHints::Performance,
    }
}

/// The set of wgpu optional features that are used: currently none
pub const FEATURES: wgpu::Features = wgpu::Features::empty();
pub const BATCHED_SPRITES_FEATURES: wgpu::Features = wgpu::Features::TEXTURE_BINDING_ARRAY
    .union(wgpu::Features::SAMPLED_TEXTURE_AND_STORAGE_BUFFER_ARRAY_NON_UNIFORM_INDEXING);

pub fn limits_for_batched_sprites(adapter: &wgpu::Adapter) -> wgpu::Limits {
    let adapter_limits = adapter.limits();
    wgpu::Limits {
        max_storage_buffers_per_shader_stage: 1,
        max_storage_buffer_binding_size: adapter_limits.max_storage_buffer_binding_size,
        max_sampled_textures_per_shader_stage: adapter_limits.max_sampled_textures_per_shader_stage,
        ..limits(adapter)
    }
}

pub fn limits(adapter: &wgpu::Adapter) -> wgpu::Limits {
    let adapter_limits = adapter.limits();
    wgpu::Limits {
        max_texture_dimension_1d: adapter_limits.max_texture_dimension_1d,
        max_texture_dimension_2d: adapter_limits.max_texture_dimension_2d,
        max_texture_dimension_3d: 256,
        max_texture_array_layers: 256,
        max_bind_groups: 1,
        max_bindings_per_bind_group: 6, // TODO: Figure out actual value
        max_dynamic_uniform_buffers_per_pipeline_layout: 0,
        max_dynamic_storage_buffers_per_pipeline_layout: 0,
        max_sampled_textures_per_shader_stage: 2,
        max_samplers_per_shader_stage: 1,
        max_storage_buffers_per_shader_stage: 0,
        max_storage_textures_per_shader_stage: 0,
        max_uniform_buffers_per_shader_stage: 3,
        max_uniform_buffer_binding_size: adapter_limits.max_uniform_buffer_binding_size,
        max_storage_buffer_binding_size: 0,
        max_vertex_buffers: 2,
        // Max layout size of all vertex shader parameters
        max_buffer_size: adapter_limits.max_buffer_size,
        // Currently largest vertex struct
        max_vertex_attributes: 9,
        max_vertex_buffer_array_stride: 96,
        min_uniform_buffer_offset_alignment: 256,
        min_storage_buffer_offset_alignment: 256,
        // Maximum [[location(n)]] (if in the vertex shader, then same as max_vertex_attributes)
        max_inter_stage_shader_components: 9, // TODO: why 9???
        max_color_attachments: 8,             // Copied from webgl2 defaults
        max_color_attachment_bytes_per_sample: 32, // Copied frmo webgl2 defaults
        max_compute_workgroup_storage_size: 0,
        max_compute_invocations_per_workgroup: 0,
        max_compute_workgroup_size_x: 0,
        max_compute_workgroup_size_y: 0,
        max_compute_workgroup_size_z: 0,
        max_compute_workgroups_per_dimension: 0,
        min_subgroup_size: 0,
        max_subgroup_size: 0,
        max_push_constant_size: 0,
        max_non_sampler_bindings: 1,
    }
}
