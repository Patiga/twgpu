use std::f32::consts::PI;
use vek::{Mat3, Vec2, Vec3};

// This program calculates the UV transformation matrices for the tilemap shader
fn main() {
    let shift: Mat3<f32> = Mat3::translation_2d(Vec2::new(-0.5, -0.5));
    let flip_x: Mat3<f32> = Mat3::with_diagonal(Vec3::new(-1., 1., 1.));
    let flip_y: Mat3<f32> = Mat3::with_diagonal(Vec3::new(1., -1., 1.));
    let mut rot: Mat3<f32> = Mat3::rotation_z(-PI / 2.);
    rot.cols.x.x = 0.;
    rot.cols.y.y = 0.;
    let back: Mat3<f32> = Mat3::translation_2d(Vec2::new(0.5, 0.5));
    println!("Rot:\n{}", rot);
    println!("FlipX:\n{}", flip_x);
    println!("FlipY:\n{}", flip_y);
    println!("Shift:\n{}", shift);
    println!("Back:\n{}", back);
    println!("Full:\n{}", back * flip_x * shift);

    println!("const TILE_UV_TRANSFORMS = array<mat3x3<f32>, 8>(");
    for i in 0..8 {
        let mut mat: Mat3<f32> = Mat3::identity();
        mat *= back;
        if i & 1 != 0 {
            mat *= flip_x;
        }
        if i & 2 != 0 {
            mat *= flip_y;
        }
        if i & 4 != 0 {
            mat *= rot;
        }
        mat *= shift;

        let elements = mat.into_col_array().map(|f| format!("{}.0", f)).join(", ");
        println!("    mat3x3<f32>({elements}),");
    }
    println!(");");
}
