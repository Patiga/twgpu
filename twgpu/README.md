TwMap
===

Render [Teeworlds](https://teeworlds.com/) and [DDNet](https://ddnet.org/) maps anywhere!

This library is written with [wgpu](https://wgpu.rs/), which runs natively on its backends Vulkan, DirectX, Metal, OpenGL and WebGL.

Usage
---

- Add `twgpu = 0.3.0` in your `Cargo.toml`
- Check out the [docs](https://docs.rs/twgpu/latest/twgpu/)

Features
---

- Gameplay rendering with [twsnap](https://gitlab.com/ddnet-rs/twsnap) is work-in-progress, but quite far
- Map rendering is fully implemented
- Tilemap rendering is done with a different method that allows rendering them with the same speed on every zoom level.
The render speed depends on the amount of pixels, however tilemaps that don't cover the whole map are optimized

Limitations
---

- Tilemaps are uploaded to the gpu as textures, they can't be bigger than the maximum texture size in that context

