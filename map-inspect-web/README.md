Map Inspect (Web)
===

Powered by the [twgpu](https://crates.io/crates/twgpu) library!

Overview
---

This is a straight forward map rendering tool that runs on the web.
There is a hosted instance on https://mapview.patiga.eu/.

- The main page will simply default to some map
- Official DDNet maps can be viewed via the url parameter `ddnet`, for example https://mapview.patiga.eu/?ddnet=Lavender%20Forest
- DDNet-Testing maps via the url parameter `ddnet-testing`
- Maps accessible via a link, with cross-origin enabled, can be accessed with the parameter `url`, for example https://mapview.patiga.eu/?url=https://heinrich5991.de/teeworlds/maps/maps/Animations_c2aa2208a9f27fcfd8349f34cf1633c038c17bdedbc225c1cdd14d4ecc19c3c0.map -
Note that many sources will not work because they have cross-origin disabled
- Maps that are stored on the server running the tool can be accessed via the url parameter `map`

While the map is loaded, the page will be white.
A persistent white screen currently means that some sort of error occurred, you can check the browser console for more info.

Mobile devices *should* be supported with nice touch support, however I would expect it to not work so well on some devices.

Setup
---

You need [Rust](https://www.rust-lang.org/tools/install) and [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/) installed on your system.

In this directory:

1. Run `wasm-pack build --target web` 
2. Create a directory `mapres_06` and fill it with all 0.6 mapres
3. Create a directory `mapres_07` and fill it with all 0.7 mapres
4. Create a directory `maps` and put a `default.map` in it, along with other maps you want to host
5. Run a http server (for local testing a simple one is `python -m http.server`)
