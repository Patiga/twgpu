use image::codecs::png::PngDecoder;
use image::{ColorType, ImageDecoder, RgbaImage};
use std::rc::Rc;
use twmap::{EmbeddedImage, GameLayer, Image, LayerKind, LoadMultiple, TwMap, Version};
use vek::Vec2;
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use web_sys::Response;
use wgpu::{
    Backends, Color, CommandEncoderDescriptor, CompositeAlphaMode, Instance, InstanceDescriptor,
    LoadOp, Operations, PowerPreference, PresentMode, RenderPassColorAttachment,
    RenderPassDescriptor, RequestAdapterOptions, StoreOp, SurfaceConfiguration, TextureFormat,
    TextureUsages, TextureViewDescriptor,
};
use winit::dpi::{PhysicalPosition, PhysicalSize};
use winit::event::{Event, MouseScrollDelta, WindowEvent};
use winit::event_loop::EventLoop;
use winit::platform::web::WindowExtWebSys;
use winit::window::{Window, WindowAttributes};

use twgpu::map::{GpuMapDataDyn, GpuMapDataStatic, GpuMapStatic};
use twgpu::textures::Samplers;
use twgpu::{device_descriptor, Camera, GpuCamera, TwRenderPass};

mod input_handler;

use crate::input_handler::{Cursors, Input, MultiInput};

const FORMAT: TextureFormat = TextureFormat::Rgba8Unorm;
const LABEL: Option<&str> = Some("Map Inspect Web");

async fn fetch_data(url: &str) -> Result<Vec<u8>, JsValue> {
    let window = web_sys::window().unwrap();
    let response = JsFuture::from(window.fetch_with_str(url)).await?;
    assert!(response.is_instance_of::<Response>());
    let response: Response = response.dyn_into()?;
    let js_array = JsFuture::from(response.array_buffer().unwrap()).await?;
    Ok(js_sys::Uint8Array::new(&js_array).to_vec())
}

async fn load_external_image(image: &mut Image, version: Version) -> Result<(), JsValue> {
    if let Image::External(ex) = image {
        let version = match version {
            Version::DDNet06 => "06",
            Version::Teeworlds07 => "07",
        };
        let url = format!("mapres_{}/{}.png", version, ex.name);
        let data = fetch_data(&url).await?;
        let image_decoder = PngDecoder::new(data.as_slice()).unwrap();
        assert_eq!(image_decoder.color_type(), ColorType::Rgba8);
        let mut image_buffer = vec![0_u8; image_decoder.total_bytes() as usize];
        let (width, height) = image_decoder.dimensions();
        image_decoder.read_image(&mut image_buffer).unwrap();
        let rgba_image = RgbaImage::from_vec(width, height, image_buffer).unwrap();
        *image = Image::Embedded(EmbeddedImage {
            name: image.name().clone(),
            image: rgba_image.into(),
        });
    }
    Ok(())
}

#[wasm_bindgen(start)]
pub async fn main() -> Result<(), JsValue> {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("Could not initialize logger");

    let doc = web_sys::window()
        .and_then(|win| win.document())
        .expect("No document in index.js");
    let event_loop = EventLoop::new().unwrap();
    let window = event_loop
        .create_window(WindowAttributes::default())
        .unwrap();
    let canvas = window.canvas().unwrap();
    canvas
        .set_attribute("style", "width: 100%; height: 100%;")
        .unwrap();
    doc.body()
        .expect("No body in document")
        .append_child(&canvas)
        .unwrap();
    let search_params = web_sys::Url::new(&doc.url()?)?.search_params();
    let mut map = None;
    if let Some(map_name) = search_params.get("map") {
        let data = fetch_data(&format!("maps/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(map_name) = search_params.get("ddnet") {
        let data = fetch_data(&format!("https://ddnet.org/mappreview/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(map_name) = search_params.get("ddnet-testing") {
        let data = fetch_data(&format!("https://ddnet.org/testmaps/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(url) = search_params.get("url") {
        let data = fetch_data(&url).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?);
    }
    let mut map = match map {
        Some(map) => map,
        None => {
            let data = fetch_data("maps/default.map").await?;
            TwMap::parse(&data).map_err(|err| err.to_string())?
        }
    };

    map.images.load().map_err(|err| err.to_string())?;
    // Tiles layer for the rendering
    // Game layer for the camera start position
    map.groups
        .load_conditionally(|layer| [LayerKind::Tiles, LayerKind::Game].contains(&layer.kind()))
        .map_err(|err| err.to_string())?;
    futures::future::join_all(
        map.images
            .iter_mut()
            .map(|image| load_external_image(image, map.version)),
    )
    .await
    .into_iter()
    .collect::<Result<_, _>>()?;
    wasm_bindgen_futures::spawn_local(run(window, event_loop, map));
    Ok(())
}

fn start_position(map: &mut TwMap) -> Vec2<f32> {
    let game_tiles = map
        .find_physics_layer::<GameLayer>()
        .unwrap()
        .tiles
        .unwrap_ref();
    let mut spawns_sum = Vec2::zero();
    let spawns_amount = game_tiles
        .indexed_iter()
        .filter(|(_, tile)| (192..=194).contains(&tile.id))
        .map(|((y, x), _)| spawns_sum += Vec2::new(x, y).az::<f32>())
        .count();
    if spawns_amount == 0 {
        Vec2::zero()
    } else {
        spawns_sum / spawns_amount as f32
    }
}

async fn run(window: Window, event_loop: EventLoop<()>, mut map: TwMap) {
    let PhysicalSize { width, height } = window.inner_size();
    let mut camera = Camera::new(width as f32 / height as f32);
    let mut old_camera = camera;
    camera.position = start_position(&mut map);

    let instance = Instance::new(&InstanceDescriptor {
        backends: Backends::all(),
        ..InstanceDescriptor::default()
    });
    let window = Rc::new(window);
    let surface = instance.create_surface(window.clone()).unwrap();
    let adapter = instance
        .request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        })
        .await
        .expect("No suitable adapter found");

    log::info!("Adapter {:?}", adapter.limits());
    log::info!("Supported features: {:?}", adapter.features());

    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .await
        .unwrap();

    let mut render_size: Vec2<f32> = Vec2::new(width, height).az();
    let mut config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format: FORMAT,
        width,
        height,
        present_mode: PresentMode::AutoVsync,
        desired_maximum_frame_latency: 0,
        alpha_mode: CompositeAlphaMode::Auto,
        view_formats: Vec::new(),
    };
    surface.configure(&device, &config);

    let gpu_camera = GpuCamera::upload(&camera, &device);
    let samplers = Samplers::new(&device);
    let map_static = GpuMapStatic::new(FORMAT, &device);
    let map_data_static = GpuMapDataStatic::upload(&map, &device, &queue);
    let map_data_dyn = GpuMapDataDyn::upload(&map, &device);
    let map_render = map_static.prepare_render(
        &map,
        &map_data_static,
        &map_data_dyn,
        &gpu_camera,
        &samplers,
        &device,
    );

    let perf_timer = web_sys::window()
        .expect("Web Sys can't find a window")
        .performance()
        .expect("Web Sys window doesnt have performance");

    let mut fps = 0;
    let mut last_fps_stat = perf_timer.now();

    let mut inputs = MultiInput::default();
    let mut cursors = Cursors::default();

    event_loop
        .run(move |event, target| {
            match event {
                Event::WindowEvent {
                    event: window_event,
                    ..
                } => match window_event {
                    WindowEvent::Resized(size) => {
                        log::info!("Resized: {}x{}", size.width, size.height);
                        render_size = Vec2::new(size.width, size.height).az();
                        config.width = size.width;
                        config.height = size.height;
                        surface.configure(&device, &config);
                        camera.switch_aspect_ratio(render_size.x / render_size.y);
                        inputs.update_map_positions(&camera);
                    }
                    WindowEvent::Touch(touch) => {
                        inputs.update_input(&Input::from_touch(touch), &mut camera, render_size)
                    }
                    WindowEvent::CursorLeft { device_id } => cursors.left(device_id),
                    WindowEvent::CursorEntered { device_id } => cursors.entered(device_id),
                    WindowEvent::CursorMoved {
                        device_id,
                        position,
                    } => {
                        if let Some(input) = cursors.moved(device_id, position) {
                            inputs.update_input(&input, &mut camera, render_size);
                        }
                    }
                    WindowEvent::MouseInput {
                        device_id,
                        state,
                        button,
                    } => {
                        if let Some(input) = cursors.input(device_id, state, button) {
                            inputs.update_input(&input, &mut camera, render_size);
                        }
                    }
                    WindowEvent::MouseWheel { delta, .. } => {
                        let zoom_out = match delta {
                            MouseScrollDelta::LineDelta(_, dy) => dy.is_sign_positive(),
                            MouseScrollDelta::PixelDelta(PhysicalPosition { y, .. }) => {
                                y.is_sign_positive()
                            }
                        };
                        if zoom_out {
                            camera.zoom /= 1.1;
                        } else {
                            camera.zoom *= 1.1;
                        }
                    }
                    WindowEvent::CloseRequested => target.exit(),
                    WindowEvent::RedrawRequested => {
                        // Only write fps once every 5 seconds
                        if perf_timer.now() - last_fps_stat >= 5000. {
                            last_fps_stat = perf_timer.now();
                            log::info!("Fps: {}", fps as f32 / 5.);
                            fps = 0;
                        }
                        fps += 1;
                        let time = (perf_timer.now() * 1000.) as i64;
                        inputs.update_camera(
                            &mut camera,
                            &old_camera,
                            render_size,
                            cursors.any_position(),
                        );
                        gpu_camera.update(&camera, &queue);
                        map_data_dyn.update(
                            &map,
                            &map_data_static,
                            &camera,
                            render_size.az(),
                            time,
                            time,
                            &queue,
                        );
                        if let Ok(frame) = surface.get_current_texture() {
                            let frame_view =
                                frame.texture.create_view(&TextureViewDescriptor::default());
                            let mut command_encoder = device
                                .create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                            {
                                let render_pass =
                                    command_encoder.begin_render_pass(&RenderPassDescriptor {
                                        label: LABEL,
                                        color_attachments: &[Some(RenderPassColorAttachment {
                                            view: &frame_view,
                                            resolve_target: None,
                                            ops: Operations {
                                                load: LoadOp::Clear(Color {
                                                    r: 0.0,
                                                    g: 0.0,
                                                    b: 0.0,
                                                    a: 1.0,
                                                }),
                                                store: StoreOp::Store,
                                            },
                                        })],
                                        depth_stencil_attachment: None,
                                        timestamp_writes: None,
                                        occlusion_query_set: None,
                                    });
                                let mut tw_render_pass =
                                    TwRenderPass::new(render_pass, render_size.az(), &camera);
                                map_render.render_background(&mut tw_render_pass);
                                map_render.render_foreground(&mut tw_render_pass);
                            }
                            queue.submit([command_encoder.finish()]);
                            frame.present();
                            window.request_redraw();
                            old_camera = camera;
                        }
                    }
                    _ => {}
                },

                _ => (),
            }
        })
        .unwrap();
}
