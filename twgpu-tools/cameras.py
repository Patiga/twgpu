import itertools
import sys

def main():
    import argparse
    parser = argparse.ArgumentParser(description="Generate camera.json for twgpu-encode-video")
    parser.add_argument("width", metavar="WIDTH", type=int, help="Map width")
    parser.add_argument("height", metavar="HEIGHT", type=int, help="Map height")
    parser.add_argument("batch_size", metavar="BATCH_SIZE", type=int, help="Number of screenshots to generate simultaneously")
    args = parser.parse_args()

    print("cargo run --bin twgpu-encode-demo --release -- --cameras cameras.json --images --resolution 2048x2048 --map <MAP> <POSES> out/", file=sys.stderr)

    horizontal_step = 32
    vertical_step = 32

    y_start = vertical_step // 2
    y_end = args.height + vertical_step // 2
    x_start = horizontal_step // 2
    x_end = args.width + horizontal_step // 2

    coords = list(itertools.product(
        range(y_start, y_end, vertical_step),
        range(x_start, x_end, horizontal_step),
    ))

    print('[')
    for n, i in enumerate(range(0, len(coords), args.batch_size)):
        tick = n + 50
        for coord_n, (y, x) in enumerate(coords[i:i+args.batch_size]):
            if n != 0 or coord_n != 0:
                print(",")
            print(
                f'{{"name":"momentcap.{y}.{x}","video":"momentcap.{y}.{x}.mkv","states":['
                f'{{"tick":{tick},"x":{x},"y":{y},"width":{horizontal_step}}}'
                ']}',
                end=""
            )
    print()
    print(']')

if __name__ == "__main__":
    main()
