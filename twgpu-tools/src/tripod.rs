use std::{error::Error, path::Path};

use serde::{Deserialize, Serialize};
use vek::Vec2;

#[derive(Clone, Copy, Serialize, Deserialize)]
struct TripodState {
    tick: f64,
    x: f32,
    y: f32,
    width: f64,
}

impl TripodState {
    fn into_camera(self, aspect_ratio: f32) -> twgpu::Camera {
        let mut camera = twgpu::Camera::new(aspect_ratio);
        let zoom = self.width as f32 / camera.base_dimensions.x;
        camera.zoom = Vec2::broadcast(zoom);
        camera.position = Vec2::new(self.x, self.y);
        camera
    }
}

#[derive(Serialize, Deserialize)]
pub struct Tripod {
    pub name: String,
    pub video: String,
    states: Vec<TripodState>,
}

pub struct TripodPool {
    video_paths: Vec<String>,
    frames: Vec<FrameInfo>,
}

#[derive(Copy, Clone)]
pub struct FrameInfo {
    /// Local index of the frames rendered in this thread, starts at 0.
    pub frame_index: i64,
    /// Global index of the frames rendered, starts above 0 if we skip frames.
    /// TODO: Also implement this for non-tripod rendering.
    pub image_index: usize,
    pub camera_index: usize,
    pub tick: f64,
    pub camera: twgpu::Camera,
    pub last_frame: bool,
}

impl TripodPool {
    pub fn video_paths(&self) -> impl Iterator<Item = &Path> {
        self.video_paths.iter().map(|p| p.as_ref())
    }

    /// This methods chunks together FrameInfos with the same tick.
    pub fn iter_frames(&self) -> impl Iterator<Item = &[FrameInfo]> {
        self.frames.chunk_by(|f1, f2| f1.tick == f2.tick)
    }

    pub fn deserialize(
        slice: &[u8],
        start_tick: Option<i32>,
        end_tick: Option<i32>,
        aspect_ratio: f32,
    ) -> Result<Self, Box<dyn Error>> {
        let tripods: Vec<Tripod> = serde_json::from_slice(slice)?;
        let mut video_paths = Vec::new();
        let mut frames = Vec::new();

        for (camera_index, tripod) in tripods.into_iter().enumerate() {
            video_paths.push(tripod.video);
            for (state_index, (image_index, state)) in tripod
                .states
                .into_iter()
                .enumerate()
                .filter(|(_, state)| {
                    start_tick.map_or(true, |st| st as f64 <= state.tick)
                        && end_tick.map_or(true, |et| et as f64 >= state.tick)
                })
                .enumerate()
            {
                frames.push(FrameInfo {
                    frame_index: state_index.try_into().unwrap(),
                    image_index,
                    camera_index,
                    tick: state.tick,
                    camera: state.into_camera(aspect_ratio),
                    last_frame: false,
                })
            }
            if let Some(last_frame) = frames.last_mut() {
                last_frame.last_frame = true;
            }
        }

        frames.sort_unstable_by(|frame1, frame2| frame1.tick.partial_cmp(&frame2.tick).unwrap());

        Ok(Self {
            video_paths,
            frames,
        })
    }
}
