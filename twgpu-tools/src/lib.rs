mod momentcap;

pub mod tripod;

use std::collections::HashSet;
use std::error::Error;
use std::io::{BufReader, Read};
use std::path::Path;
use std::rc::Rc;
use std::str::FromStr;
use std::sync::Arc;
use std::{fs, mem, ops};

use image::{ImageFormat, RgbaImage};
use teehistorian::ThStream;
use teehistorian_replayer::{teehistorian, twgame_core};
use twgame::Map;
use twgame_core::Snapper;
use twgpu::blit::Blit;
use twgpu::buffer::GpuVecBuffer;
use twgpu::map::{GpuMapDataDyn, GpuMapDataStatic, GpuMapRender, GpuMapStatic};
use twgpu::shared::{Clock, Rng};
use twgpu::sprites::{
    AtlasToken, Particles, SpriteRenderContext, SpriteTextures, SpriteVertex, TeeSprite,
    TeeStorage, TextureToken,
};
use twgpu::textures::Samplers;
use twgpu::GpuCamera;
use twmap::{LoadMultiple, TwMap};
use twsnap::compat::ddnet::{DemoChunk, DemoMapHash, DemoReader};
use vek::Vec2;
use wgpu::{Device, Queue, Texture, TextureFormat, TextureView, TextureViewDescriptor};

pub type SpriteBuffer = GpuVecBuffer<[SpriteVertex; 4]>;
pub type TextureBuffer = GpuVecBuffer<TextureToken>;

pub fn parse_tuple<T: FromStr, const SEPARATOR: char>(
    s: &str,
) -> Result<(T, T), Box<dyn Error + Send + Sync + 'static>>
where
    T::Err: Error + Send + Sync + 'static,
{
    if s.matches(SEPARATOR).count() != 1 {
        return Err(format!("Expected 2 values separated by '{SEPARATOR}'").into());
    }
    let mut values = s.split(SEPARATOR).map(|str| str.parse::<T>());
    Ok((values.next().unwrap()?, values.next().unwrap()?))
}

pub fn gpu_map_from_path(
    path: &Path,
    map_static: &GpuMapStatic,
    camera: &GpuCamera,
    samplers: &Samplers,
    device: &Device,
    queue: &Queue,
) -> Result<(twmap::TwMap, GpuMapDataStatic, GpuMapDataDyn, GpuMapRender), Box<dyn Error>> {
    println!("Loading map");
    let mut map = twmap::TwMap::parse_path(path)?;
    map.embed_images_auto()?;
    map.images.load()?;
    map.groups
        .load_conditionally(|layer| layer.kind() == twmap::LayerKind::Tiles)?;
    println!("Uploading map data to GPU");
    let map_static_data = GpuMapDataStatic::upload(&map, device, queue);
    let map_dyn_data = GpuMapDataDyn::upload(&map, device);
    let map_render = map_static.prepare_render(
        &map,
        &map_static_data,
        &map_dyn_data,
        camera,
        samplers,
        device,
    );
    Ok((map, map_static_data, map_dyn_data, map_render))
}

pub struct DownloadTexture {
    texture: wgpu::Texture,
    download_buffer: wgpu::Buffer,
    width: u32,
    height: u32,
    buffer_size: u64,
    bytes_per_row: u32,
    padded_bytes_per_row: u32,
}

impl DownloadTexture {
    pub fn new(width: u32, height: u32, format: TextureFormat, device: &wgpu::Device) -> Self {
        assert_ne!(width, 0);
        assert_ne!(height, 0);
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Render Target Texture"),
            size: wgpu::Extent3d {
                width,
                height,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::COPY_SRC,
            view_formats: &[],
        });
        let bytes_per_pixel: u32 = format.block_copy_size(None).unwrap();
        let bytes_per_row: u32 = width * bytes_per_pixel;
        let align: u32 = wgpu::COPY_BYTES_PER_ROW_ALIGNMENT;
        let padding: u32 = (align - (bytes_per_row % align)) % align;
        let padded_bytes_per_row: u32 = bytes_per_row + padding;
        let buffer_size: u64 = u64::from(padded_bytes_per_row) * u64::from(height);
        let download_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Image Download Buffer"),
            size: buffer_size,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        Self {
            texture,
            download_buffer,
            width,
            height,
            buffer_size,
            bytes_per_row,
            padded_bytes_per_row,
        }
    }

    pub fn texture_view(&self) -> TextureView {
        self.texture.create_view(&TextureViewDescriptor::default())
    }

    pub fn map<T: FnOnce() + Send + 'static>(
        &self,
        on_map: T,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
    ) {
        let mut command_encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Download Texture Copy"),
        });
        command_encoder.copy_texture_to_buffer(
            self.texture.as_image_copy(),
            wgpu::TexelCopyBufferInfo {
                buffer: &self.download_buffer,
                layout: wgpu::TexelCopyBufferLayout {
                    offset: 0,
                    bytes_per_row: Some(self.padded_bytes_per_row),
                    rows_per_image: Some(self.height),
                },
            },
            wgpu::Extent3d {
                width: self.width,
                height: self.height,
                depth_or_array_layers: 1,
            },
        );
        let buffer_slice = self.download_buffer.slice(..);
        queue.submit([command_encoder.finish()]);
        buffer_slice.map_async(wgpu::MapMode::Read, |e| {
            e.unwrap();
            on_map()
        });
        device.poll(wgpu::Maintain::Wait);
    }

    pub fn download_mapped<T: FnMut(&[u8])>(&self, row_closure: T) {
        {
            let downloaded = self.download_buffer.slice(..).get_mapped_range();
            assert_eq!(downloaded.len(), self.buffer_size as usize);
            downloaded
                .chunks_exact(self.padded_bytes_per_row as usize)
                .map(|padded_row| &padded_row[..self.bytes_per_row as usize])
                .for_each(row_closure);
        }
        self.download_buffer.unmap();
    }

    pub fn download_rgba(&self, device: &wgpu::Device, queue: &wgpu::Queue) -> image::RgbaImage {
        assert_eq!(self.texture.format(), TextureFormat::Rgba8Unorm);
        self.map(|| {}, device, queue);
        self.download_mapped_rgba()
    }

    pub fn download_mapped_rgba(&self) -> image::RgbaImage {
        let mut image_buffer =
            Vec::with_capacity(self.bytes_per_row as usize * self.height as usize);
        self.download_mapped(|row| image_buffer.extend(row));
        image::RgbaImage::from_vec(self.width, self.height, image_buffer).unwrap()
    }
}

pub fn load_png(path: &str, version: twmap::Version) -> Result<RgbaImage, Box<dyn Error>> {
    let file = twstorage::read_file(path, version.into())?;
    let reader = image::io::Reader::with_format(BufReader::new(file), ImageFormat::Png);
    Ok(reader.decode()?.into_rgba8())
}

pub fn init_sprite_textures(
    textures: &mut SpriteTextures,
    version: twmap::Version,
    device: &Device,
    queue: &Queue,
) -> Result<(), Box<dyn Error>> {
    println!("Loading sprite textures from filesystem");
    let game = load_png("game.png", version)?;
    let particles = load_png("particles.png", version)?;
    let emoticons = load_png("emoticons.png", version)?;
    let extras = load_png("extras.png", version)?;
    textures.game_skin = textures.register_atlas_image(&game, device, queue);
    textures.particles = textures.register_atlas_image(&particles, device, queue);
    textures.emoticons = textures.register_atlas_image(&emoticons, device, queue);
    textures.extras = textures.register_atlas_image(&extras, device, queue);
    Ok(())
}

type SkinResult = Result<(String, Vec<Texture>), (String, String)>;

pub struct SkinManager {
    known: HashSet<String>,
    handles: Vec<std::thread::JoinHandle<SkinResult>>,
    blit: Arc<Blit>,
    device: Arc<Device>,
    queue: Arc<Queue>,
}

fn load_skin(
    name: &str,
    blit: &Blit,
    device: &Device,
    queue: &Queue,
) -> Result<Vec<Texture>, Box<dyn Error>> {
    let options = sanitize_filename::OptionsForCheck {
        windows: true,
        truncate: true,
    };
    if !sanitize_filename::is_sanitized_with_options(name, options) {
        return Err(format!("Skin name '{name}' is not sanitized").into());
    }
    let path = format!("skins/{name}.png");
    let path2 = format!("downloadedskins/{name}.png");
    let image = match load_png(&path, twmap::Version::DDNet06) {
        Err(_) => load_png(&path2, twmap::Version::DDNet06)?,
        Ok(img) => img,
    };

    let texture = blit.upload_mipmapped_atlas::<TeeSprite>(&image, device, queue);
    Ok(texture)
}

impl SkinManager {
    pub fn new(
        blit: Arc<Blit>,
        textures: &mut SpriteTextures,
        device: Arc<Device>,
        queue: Arc<Queue>,
    ) -> Self {
        let mut manager = Self {
            known: HashSet::new(),
            handles: Vec::new(),
            blit,
            device,
            queue,
        };
        if let Some(default) = manager.load_skin("default", textures) {
            textures.default_skin = default;
        }
        if let Some(ninja) = manager.load_skin("x_ninja", textures) {
            textures.ninja_skin = ninja;
        }
        manager
    }

    pub fn load_skin(
        &mut self,
        name: &str,
        textures: &mut SpriteTextures,
    ) -> Option<AtlasToken<TeeSprite>> {
        let texture = match load_skin(name, &self.blit, &self.device, &self.queue) {
            Ok(texture) => texture,
            Err(err) => {
                println!("Error loading skin '{name}': {err}");
                return None;
            }
        };
        self.known.insert(name.to_string());
        let token = textures.register_skin_texture(texture, name.to_string(), &self.device);
        Some(token)
    }

    pub fn queue_load_skin(&mut self, name: String) {
        let device = self.device.clone();
        let queue = self.queue.clone();
        let blit = self.blit.clone();
        let handle = std::thread::spawn(move || match load_skin(&name, &blit, &device, &queue) {
            Err(err) => Err((name, err.to_string())),
            Ok(texture) => Ok((name, texture)),
        });
        self.handles.push(handle);
    }

    pub fn wait_for_queued(&mut self, textures: &mut SpriteTextures) {
        while let Some(handle) = self.handles.pop() {
            match handle.join().unwrap() {
                Ok((name, texture)) => {
                    textures.register_skin_texture(texture, name, &self.device);
                }
                Err((name, err)) => println!("Error with skin '{name}': {err}"),
            }
        }
    }

    pub fn poll_queued(&mut self, textures: &mut SpriteTextures) {
        while !self.handles.is_empty() {
            if self.handles[0].is_finished() {
                match self.handles.remove(0).join().unwrap() {
                    Ok((name, texture)) => {
                        textures.register_skin_texture(texture, name, &self.device);
                    }
                    Err((name, err)) => println!("Error with skin '{name}': {err}"),
                }
            } else {
                return;
            }
        }
    }

    pub fn queue_snap_skins(&mut self, snap: &twsnap::Snap) {
        for player in snap.players.values() {
            if !self.known.contains(player.skin.as_str()) {
                println!("New skin: '{}'", player.skin);
                self.known.insert(player.skin.to_string());
                self.queue_load_skin(player.skin.to_string());
            }
        }
    }
}

pub struct DemoProcessor {
    demo: Box<dyn DemoLike>,
    clock: Clock,
    map: Rc<Map>,
    from_snap: twsnap::Snap,
    to_snap: twsnap::Snap,
    tees: TeeStorage,
    new_snap: bool,
}

fn maps(data: &[u8]) -> Result<(TwMap, Rc<Map>), Box<dyn Error>> {
    let mut twmap_map = TwMap::parse(&data)?;
    twmap_map.embed_images_auto()?;
    twmap_map.load()?;
    let twgame_map = Rc::new(twgame::Map::try_from(&mut twmap_map)?);
    Ok((twmap_map, twgame_map))
}

fn file_path_to_vec(path: &Path) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut buf = Vec::new();
    let mut file = fs::File::open(path)?;
    file.read_to_end(&mut buf)?;
    Ok(buf)
}

fn retrieve_maps(name: &str, sha: Option<&str>) -> Result<(TwMap, Rc<Map>), Box<dyn Error>> {
    let version = twstorage::Version::DDNet06;
    if let Some(sha) = sha {
        let path = format!("downloadedmaps/{name}_{sha}.map");
        if let Ok(mut file) = twstorage::read_file(&path, version) {
            let mut data = Vec::new();
            file.read_to_end(&mut data)?;
            let map = maps(&data)?;
            return Ok(map);
        }
    }
    let path = format!("maps/{name}.map");
    if let Ok(mut file) = twstorage::read_file(&path, version) {
        let mut data = Vec::new();
        file.read_to_end(&mut data)?;
        let map = maps(&data)?;
        return Ok(map);
    }
    Err("Map not embedded in demo and its file couldn't be located".into())
}

impl DemoProcessor {
    pub fn new_by_extension(
        path: &Path,
        map: Option<&Path>,
    ) -> Result<(Self, TwMap, Rc<Map>), Box<dyn Error>> {
        let file = fs::File::open(path)?;
        let map = match map {
            None => None,
            Some(path) => Some(maps(&file_path_to_vec(path)?)?),
        };
        let extension = path.extension().and_then(|os| os.to_str());
        if extension == Some("teehistorian") {
            let (demo, twmap, map) = TeehistorianDemo::new(file, map)?;
            let demo = Self::new(Box::new(demo), map.clone())?;
            Ok((demo, twmap, map))
        } else if extension == Some("poses") {
            let demo = MomentcapDemo::new(file)?;
            let (twmap, map) = map.ok_or("Need explicit map file for momentcap file")?;
            let demo = Self::new(Box::new(demo), map.clone())?;
            Ok((demo, twmap, map))
        } else {
            // assume demo
            if extension != Some("demo") {
                println!("Unrecognized demo file extension, defaulting to normal demo");
            }
            let demo = DemoReader::new(file)?;
            let (twmap, map) = match map {
                Some(map) => map,
                None => {
                    if let Some(data) = demo.map_data() {
                        maps(data)?
                    } else {
                        let map_name = demo.map_name();
                        let map_sha = match demo.map_hash() {
                            DemoMapHash::Crc(_) => None,
                            DemoMapHash::Sha256(sha) => {
                                Some(sha.map(|byte| format!("{byte:02x}")).join(""))
                            }
                        };
                        retrieve_maps(map_name, map_sha.as_deref())?
                    }
                }
            };
            let demo = Self::new(Box::new(demo), map.clone())?;
            Ok((demo, twmap, map))
        }
    }

    pub fn new(mut demo: Box<dyn DemoLike>, map: Rc<Map>) -> Result<Self, Box<dyn Error>> {
        let mut snap = twsnap::Snap::default();
        let clock = loop {
            match demo.next_demo_chunk(&mut snap)? {
                None => break Clock::default(),
                Some(DemoChunk::NetMsg) => continue,
                Some(DemoChunk::Snapshot(tick)) => break Clock::new(tick.snap_tick()),
            }
        };
        Ok(Self {
            demo,
            clock,
            map,
            from_snap: twsnap::Snap::default(),
            to_snap: snap,
            tees: TeeStorage::default(),
            new_snap: true,
        })
    }

    pub fn camera_position(&mut self) -> Option<Vec2<f32>> {
        for (player_id, player) in self.from_snap.players.iter() {
            if player.local {
                if let Some(tees) = self.tees.lerp_tee(
                    player_id,
                    &self.clock,
                    &self.from_snap,
                    &self.to_snap,
                    &self.map,
                ) {
                    return Some(self.clock.lerp_tee_vec(tees, |tee| tee.pos));
                }
            }
        }
        None
    }

    pub fn start_tick(&self) -> i32 {
        self.clock.start_tick()
    }

    pub fn current_time(&self) -> f64 {
        self.clock.current_time()
    }

    /// Returns `Ok(None)`, if the demo is over.
    pub fn to_tick(
        &mut self,
        f_tick: f64,
        safe_tick_threshold: u32,
    ) -> Result<Option<bool>, Box<dyn Error>> {
        if self.clock.update(f_tick) {
            let new_tick = self.demo.snaps_for_tick(
                &mut self.clock,
                safe_tick_threshold,
                &mut self.from_snap,
                &mut self.to_snap,
            )?;
            if new_tick.is_some() {
                self.new_snap = true;
            }
            Ok(new_tick.map(|_| true))
        } else {
            Ok(Some(false))
        }
    }

    pub fn new_snap(&self) -> Option<&twsnap::Snap> {
        self.new_snap.then_some(&self.to_snap)
    }

    pub fn render_ctx<'a>(
        &'a mut self,
        particles: &'a mut Particles,
        textures: &'a SpriteTextures,
        rng: &'a mut Rng,
    ) -> SpriteRenderContext<'a> {
        particles.update(&self.clock, &self.map, rng);
        let mut ctx = SpriteRenderContext {
            clock: &self.clock,
            from_snap: &self.from_snap,
            to_snap: &self.to_snap,
            tees: &mut self.tees,
            map: &self.map,
            particles,
            textures,
            rng,
        };
        if self.new_snap {
            ctx.process_events();
            self.new_snap = false;
        }
        ctx
    }
}

pub trait DemoLike {
    fn next_demo_chunk(
        &mut self,
        snap: &mut twsnap::Snap,
    ) -> Result<Option<DemoChunk>, Box<dyn Error>>;

    /// This method skips to at least the given tick.
    /// It might skip further than that, though typically not too far.
    /// Returns Ok(None), if the demo is over
    fn next_demo_chunk_after(
        &mut self,
        snap: &mut twsnap::Snap,
        after: i32,
    ) -> Result<Option<DemoChunk>, Box<dyn Error>> {
        loop {
            match self.next_demo_chunk(snap)? {
                None => break Ok(None),
                Some(DemoChunk::Snapshot(tick)) => {
                    if tick.snap_tick() < after {
                        continue;
                    } else {
                        break Ok(Some(DemoChunk::Snapshot(tick)));
                    }
                }
                Some(_) => continue,
            }
        }
    }

    /// `safe_tick_threshold` is the amount of ticks, in which we expect at least 3 snapshots.
    /// This method will try to skip any amount of ticks according to `safe_tick_threshold`.
    /// And from there on out read each snap.
    /// Returns an error, if we skipped too far.
    fn snaps_for_tick(
        &mut self,
        clock: &mut Clock,
        safe_tick_threshold: u32,
        from: &mut twsnap::Snap,
        to: &mut twsnap::Snap,
    ) -> Result<Option<i32>, Box<dyn Error>> {
        if !clock.update(clock.current_tick()) {
            panic!("Already on appropriate tick!");
        }
        assert!(!clock.current_tick().is_nan());
        assert!(clock.current_tick().is_sign_positive());
        let diff = clock.current_tick() - clock.end_tick() as f64;
        assert!(diff.is_sign_positive());
        let mut skipped = false;
        let mut snaps_after_skip = 0;
        if diff as u32 > safe_tick_threshold {
            skipped = true;
            mem::swap(from, to);
            let skip_to = clock.current_tick() as u32 - safe_tick_threshold;
            match self.next_demo_chunk_after(to, skip_to.try_into().unwrap())? {
                None => return Ok(None),
                Some(DemoChunk::NetMsg) => {}
                Some(DemoChunk::Snapshot(tick)) => {
                    clock.next_tick(tick.snap_tick());
                    snaps_after_skip += 1;
                }
            }
        }
        while clock.need_next_tick() {
            match self.next_demo_chunk(from)? {
                None => return Ok(None),
                Some(DemoChunk::NetMsg) => continue,
                Some(DemoChunk::Snapshot(tick)) => {
                    mem::swap(from, to);
                    snaps_after_skip += 1;
                    clock.next_tick(tick.snap_tick());
                }
            }
        }
        if skipped && snaps_after_skip < 2 {
            return Err(
                "Too optimistic tick skip: too few snaps after skip. Adjust the skip_threshold!"
                    .into(),
            );
        }
        Ok(Some(clock.end_tick()))
    }
}

impl DemoLike for DemoReader {
    fn next_demo_chunk(
        &mut self,
        snap: &mut twsnap::Snap,
    ) -> Result<Option<DemoChunk>, Box<dyn Error>> {
        self.next_chunk(snap).map_err(Into::into)
    }
}

pub struct TeehistorianDemo {
    th: teehistorian::ThCompat<teehistorian::ThBufReader<fs::File>>,
    replayer: teehistorian_replayer::ThReplayer,
    world: twgame::DdnetReplayerWorld,
}
impl TeehistorianDemo {
    pub fn new(
        file: fs::File,
        maps: Option<(TwMap, Rc<twgame::Map>)>,
    ) -> Result<(Self, TwMap, Rc<Map>), Box<dyn Error>> {
        let buf_read = teehistorian::ThBufReader::new(file);
        let mut th = teehistorian::ThCompat::parse(buf_read)?;
        let th_header = th.header()?;
        let (twmap, map) = match maps {
            Some(maps) => maps,
            None => {
                let th_header: serde_json::Value = serde_json::from_slice(th_header)?;
                let map_name = th_header
                    .get("map_name")
                    .and_then(|val| val.as_str())
                    .ok_or("teehistorian header with no map name")?;
                let map_sha = th_header
                    .get("map_sha256")
                    .and_then(|val| val.as_str())
                    .ok_or("teehistorian header with no map sha")?;
                retrieve_maps(map_name, Some(map_sha))?
            }
        };

        let mut world = twgame::DdnetReplayerWorld::new(map.clone(), true);
        let replayer = teehistorian_replayer::ThReplayer::new(th_header, &mut world);
        Ok((
            Self {
                th,
                world,
                replayer,
            },
            twmap,
            map,
        ))
    }
}

impl DemoLike for TeehistorianDemo {
    fn next_demo_chunk(
        &mut self,
        snap: &mut twsnap::Snap,
    ) -> Result<Option<DemoChunk>, Box<dyn Error>> {
        let last_tick = self.replayer.cur_time();
        loop {
            let chunk = match self.th.next_chunk() {
                Ok(chunk) => Ok(Some(chunk)),
                Err(teehistorian::Error::Eof) => Ok(None),
                Err(err) => Err(err),
            }?;
            let teehistorian_ended = chunk.is_none();
            self.replayer
                .replay_next_chunk(&mut self.world, chunk, None);
            let tick = self.replayer.cur_time();
            if tick > last_tick {
                snap.clear();
                self.world.snap(snap);
                break Ok(Some(DemoChunk::Snapshot(tick)));
            }
            if teehistorian_ended {
                break Ok(None);
            }
        }
    }
}

pub struct MomentcapDemo {
    data: Vec<u8>,
    ticks: ops::Range<i32>,
}

impl MomentcapDemo {
    pub fn new(mut file: fs::File) -> Result<Self, Box<dyn Error>> {
        let mut result = Self {
            data: Vec::new(),
            ticks: 49..10000 + 1,
        };
        file.read_to_end(&mut result.data)?;
        result.poses()?;
        Ok(result)
    }
    fn poses(&self) -> Result<&[momentcap::Pose], Box<dyn Error>> {
        Ok(momentcap::read_poses(&self.data)?)
    }
}

impl DemoLike for MomentcapDemo {
    fn next_demo_chunk(
        &mut self,
        snap: &mut twsnap::Snap,
    ) -> Result<Option<DemoChunk>, Box<dyn Error>> {
        Ok(self.ticks.next().map(|tick| {
            let tick = twsnap::time::Instant::from_snap_tick(tick);
            momentcap::to_snap(self.poses().unwrap(), snap, tick);
            DemoChunk::Snapshot(tick)
        }))
    }
}
