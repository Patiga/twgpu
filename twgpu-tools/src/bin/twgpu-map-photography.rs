use clap::Parser;
use pollster::FutureExt;
use std::error::Error;
use std::iter;
use std::path::PathBuf;
use twmap::{LayerKind, LoadMultiple};
use vek::{Extent2, Vec2};

use twgpu::map::{GpuMapDataDyn, GpuMapDataStatic, GpuMapStatic};
use twgpu::textures::Samplers;
use twgpu::{device_descriptor, Camera, GpuCamera, TwRenderPass};
use twgpu_tools::{parse_tuple, DownloadTexture};

const FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba8Unorm;
const LABEL: Option<&str> = Some("Map Photography");

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(
    about = "Take photos of Teeworlds/DDNet maps via the command line. If multiple resolutions/zoom levels/positions are specified, photos of all permutations will be taken"
)]
struct Cli {
    /// Path to the modelling map
    #[clap(value_parser)]
    map: PathBuf,
    /// Zoom levels of the photos, 1 if no zoom level is specified.
    /// 2 -> twice as many tiles on photo, 0.5 -> half as many tiles
    #[clap(long, short = 'z', value_parser)]
    zoom: Vec<f32>,
    /// Resolution <width>x<height> of the taken photos,
    /// 1920x1080 if no resolution is specified
    #[clap(long, short = 'r', value_parser = parse_tuple::<u32, 'x'>)]
    resolution: Vec<(u32, u32)>,
    /// Position <x>,<y> of the camera, 0,0 if no position is specified
    /// 0,0 -> top-left corner of the map, 20,0.5 -> 20 tiles to the right, 0.5 tiles down
    #[clap(long, short = 'p', value_parser = parse_tuple::<f32, ','>)]
    position: Vec<(f32, f32)>,
}

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let mut cli: Cli = Cli::parse();
    let default_resolution = cli.resolution.is_empty();
    if default_resolution {
        cli.resolution.push((1920, 1080));
    }
    let default_zoom = cli.zoom.is_empty();
    if default_zoom {
        cli.zoom.push(1.);
    }

    println!("Loading map");
    let mut map = twmap::TwMap::parse_path(&cli.map)?;
    let game_layer = map.find_physics_layer::<twmap::GameLayer>().unwrap();
    let map_size: Extent2<f32> = game_layer.tiles.shape().az();
    let map_middle = map_size / 2.;
    let default_position = cli.position.is_empty();
    if default_position {
        cli.position.push((map_middle.w, map_middle.h));
    }

    map.embed_images_auto()?;
    map.images.load()?;
    map.groups
        .load_conditionally(|layer| layer.kind() == LayerKind::Tiles)?;

    println!("Connecting to GPU backend");
    let instance = wgpu::Instance::new(&wgpu::InstanceDescriptor {
        backends: wgpu::Backends::all(),
        ..wgpu::InstanceDescriptor::default()
    });
    let adapter = wgpu::util::initialize_adapter_from_env(&instance, None).unwrap_or_else(|| {
        instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: None,
            })
            .block_on()
            .expect("No suitable adapter found")
    });
    eprintln!("{:#?}", adapter.get_info());
    eprintln!("Adapter {:#?}", adapter.limits());
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .block_on()?;

    println!("Uploading data to GPU");
    let mut camera = Camera::new(1.);
    let gpu_camera = GpuCamera::upload(&camera, &device);
    let samplers = Samplers::new(&device);
    let map_static = GpuMapStatic::new(FORMAT, &device);
    let map_data_static = GpuMapDataStatic::upload(&map, &device, &queue);
    let map_data_dyn = GpuMapDataDyn::upload(&map, &device);
    let map_render = map_static.prepare_render(
        &map,
        &map_data_static,
        &map_data_dyn,
        &gpu_camera,
        &samplers,
        &device,
    );

    println!("Rendering pictures as PNG");
    for resolution in cli.resolution {
        let download_texture = DownloadTexture::new(resolution.0, resolution.1, FORMAT, &device);
        camera.switch_aspect_ratio(resolution.0 as f32 / resolution.1 as f32);
        for &zoom in &cli.zoom {
            if default_position {
                let zoom_sizes: Vec2<f32> = Vec2::<f32>::from(map_size) / camera.base_dimensions;
                camera.zoom = (zoom_sizes.x.max(zoom_sizes.y) * zoom).into();
            } else {
                camera.zoom = [zoom, zoom].into();
            }
            for &position in &cli.position {
                camera.position = position.into();
                gpu_camera.update(&camera, &queue);
                map_data_dyn.update(
                    &map,
                    &map_data_static,
                    &camera,
                    resolution.into(),
                    0,
                    0,
                    &queue,
                );
                let view = download_texture.texture_view();
                let mut command_encoder =
                    device.create_command_encoder(&wgpu::CommandEncoderDescriptor { label: LABEL });
                {
                    let render_pass =
                        command_encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                            label: LABEL,
                            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                                view: &view,
                                resolve_target: None,
                                ops: wgpu::Operations {
                                    load: wgpu::LoadOp::Clear(wgpu::Color {
                                        r: 0.0,
                                        g: 0.0,
                                        b: 0.0,
                                        a: 1.0,
                                    }),
                                    store: wgpu::StoreOp::Store,
                                },
                            })],
                            depth_stencil_attachment: None,
                            timestamp_writes: None,
                            occlusion_query_set: None,
                        });
                    let mut tw_render_pass =
                        TwRenderPass::new(render_pass, resolution.into(), &camera);
                    map_render.render_background(&mut tw_render_pass);
                    map_render.render_foreground(&mut tw_render_pass);
                }
                queue.submit(iter::once(command_encoder.finish()));
                let image = download_texture.download_rgba(&device, &queue);
                let mut image_path = cli.map.file_stem().unwrap().to_owned();
                if !default_resolution {
                    image_path.push("_");
                    image_path.push(format!("{}x{}", resolution.0, resolution.1));
                }
                if !default_position {
                    image_path.push("_");
                    image_path.push(format!("{},{}", position.0, position.1));
                }
                if !default_zoom {
                    image_path.push("_");
                    image_path.push(format!("x{zoom}"));
                }
                image_path.push(".png");
                image.save(&image_path)?;
                println!("Saved to {image_path:?}");
            }
        }
    }
    Ok(())
}
