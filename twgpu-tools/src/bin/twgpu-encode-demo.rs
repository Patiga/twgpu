use clap::{Parser, ValueEnum};
use ffmpeg::ffi::{
    av_buffer_ref, av_buffer_unref, av_hwdevice_ctx_create, av_hwframe_ctx_alloc,
    av_hwframe_ctx_init, av_hwframe_get_buffer, av_hwframe_transfer_data, AVBufferRef,
    AVHWDeviceType, AVHWFramesContext,
};
use ffmpeg::{codec, encoder, format, util};
use ffmpeg_next as ffmpeg;
use pollster::FutureExt;
use rayon::iter::{ParallelBridge, ParallelIterator};
use std::error::Error;
use std::fs;
use std::mem;
use std::ops::Range;
use std::path::{Path, PathBuf};
use std::ptr;
use std::sync::{mpsc, Arc};
use std::thread;
use twgpu::blit::Blit;
use twgpu::buffer::GpuDeviceExt;
use twgpu_tools::tripod::{FrameInfo, TripodPool};
use vek::Vec2;
use wgpu::{
    Adapter, Backends, Color, CommandEncoder, CommandEncoderDescriptor, Device, Instance,
    InstanceDescriptor, LoadOp, Operations, PowerPreference, Queue, RenderBundle,
    RenderBundleDescriptor, RenderBundleEncoder, RenderBundleEncoderDescriptor, RenderPass,
    RenderPassColorAttachment, RenderPassDescriptor, RequestAdapterOptions, StoreOp, TextureFormat,
    TextureView,
};

use twgpu::map::{GpuMapDataDyn, GpuMapDataStatic, GpuMapRender, GpuMapStatic};
use twgpu::shared::Rng;
use twgpu::sprites::{
    BatchSpriteRender, ParticleGroup, Particles, SpriteCache, SpriteGenerator, SpriteTextures,
    SpritesStatic,
};
use twgpu::textures::Samplers;
use twgpu::{
    device_descriptor, device_descriptor_for_batched_sprites, Camera, GpuCamera, TwRenderPass,
};
use twgpu_tools::{
    init_sprite_textures, parse_tuple, DemoProcessor, DownloadTexture, SkinManager, SpriteBuffer,
    TextureBuffer,
};

const TPS: f64 = 50.; // Ticks per second. TODO: make this a command line argument.
const FORMAT: TextureFormat = TextureFormat::Rgba8Unorm;
const LABEL: Option<&str> = Some("Demo Encoder");
const RENDER_FORMAT: format::Pixel = format::Pixel::RGB24;
const CONVERT_FORMAT: format::Pixel = format::Pixel::NV12;
const HW_FORMAT: format::Pixel = format::Pixel::VAAPI;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Preset {
    Ultrafast,
    Superfast,
    Veryfast,
    Faster,
    Fast,
    Medium,
    Slow,
    Slower,
    Veryslow,
    Placebo,
}

#[derive(Parser, Debug)]
#[clap(
    author,
    version,
    about = "Encode DDNet demos into common video formats"
)]
struct Cli {
    /// Demo file path
    demo: PathBuf,
    /// Output video file path, determines the video format.
    /// If cameras where specified, this sets the output directory.
    output: PathBuf,
    /// Provide or override the map
    #[arg(long)]
    map: Option<PathBuf>,
    /// The first tick to render.
    #[arg(long)]
    start_tick: Option<i32>,
    /// Stops rendering at this tick.
    #[arg(long)]
    end_tick: Option<i32>,
    /// Tick gap size where we start skipping.
    #[arg(long, default_value = "50")]
    skip_threshold: u32,
    /// Preinstalled camera movements, saved in a file
    #[arg(long)]
    cameras: Option<PathBuf>,
    /// Frames per second
    #[arg(long, value_parser, default_value = "60")]
    fps: u32,
    /// x264 encoding preset
    #[arg(long, value_enum, default_value = "slow")]
    preset: Preset,
    /// x264 encoding parameters. See `x264 --help` for
    /// parameters.
    #[arg(long, value_parser, default_value = "bframes=8")]
    params: String,
    /// Zoom levels of the photos.
    /// 2 -> twice as many tiles on photo, 0.5 -> half as many tiles
    #[arg(long, short = 'z', value_parser, default_value = "1")]
    zoom: f32,
    /// Resolution <width>x<height> of the video,
    #[arg(long, short = 'r', value_parser = parse_tuple::<u32, 'x'>, default_value = "1920x1080")]
    resolution: (u32, u32),
    /// Use hardware acceleration for the video encoding.
    /// This might not be available on your computer!
    /// This will also massively INFLATE THE FILESIZE.
    #[arg(long, value_parser)]
    vaapi: bool,
    #[arg(long)]
    max_sprites: Option<usize>,
    /// Don't encode to a video format, but to PNGs.
    #[arg(long)]
    images: bool,
    #[arg(long)]
    compatibility: bool,
}

pub struct RenderedFrame {
    camera_index: usize,
    frame: Arc<DownloadTexture>,
    frame_idx: i64,
    image_idx: usize,
    is_final_frame: bool,
}

pub struct EncodedFrame {
    pub frame: util::frame::Video,
    pub is_final_frame: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    let cli: Arc<Cli> = Arc::new(Cli::parse());
    env_logger::init();

    let mut renderer = RenderIterator::new(cli.clone())?;
    let mut encoder_threads = Vec::new();
    let mut encode_senders = Vec::new();

    ffmpeg::init().unwrap();

    let (tripods, output_paths) = match &cli.cameras {
        None => (None, vec![cli.output.clone()]),
        Some(path) => {
            let file = fs::read(path)?;
            let aspect_ratio = cli.resolution.0 as f32 / cli.resolution.1 as f32;
            let tripods =
                TripodPool::deserialize(&file, cli.start_tick, cli.end_tick, aspect_ratio)?;
            let output_paths = tripods
                .video_paths()
                .map(|name| cli.output.join(name))
                .collect();
            (Some(tripods), output_paths)
        }
    };

    // Prepare one processing channel for each camera view.
    // Each camera view gets two threads:
    // One frame processor thread and one ffmpeg encoder thread.
    // The rendering happens on the main thread, for all camera views together.
    for output_path in &output_paths {
        let output_path = output_path.to_path_buf();
        let cli = cli.clone();
        let (encode_sender, encode_receiver) = mpsc::sync_channel::<EncodedFrame>(10);

        if cli.images {
            continue;
        }
        encode_senders.push(encode_sender);

        encoder_threads.push(
            thread::Builder::new()
                .spawn(|| {
                    let receiver = encode_receiver;
                    let output_path = output_path;
                    let cli = cli;
                    let mut expected_index = 0;
                    let mut out_of_order = Vec::<(util::frame::Video, bool)>::new();

                    let mut handle_frame =
                        |encoder: &mut FfmpegEncoder, frame: util::frame::Video, is_final: bool| {
                            out_of_order.push((frame, is_final));
                            let mut finished = false;
                            while let Some(ooo) = out_of_order
                                .iter()
                                .position(|(e, _)| e.pts() == Some(expected_index))
                            {
                                let (frame, is_final_frame) = out_of_order.remove(ooo);
                                encoder.encode_frame(&frame).unwrap();
                                expected_index += 1;
                                if is_final_frame {
                                    finished = true;
                                }
                            }
                            if finished {
                                assert!(out_of_order.is_empty());
                            }
                            finished
                        };

                    // This block lazily initializes the FFMPEG encoder.
                    // And then eagerly ends it.
                    if let Ok(EncodedFrame {
                        frame,
                        is_final_frame,
                    }) = receiver.recv()
                    {
                        let mut ffmpeg = FfmpegEncoder::new(&output_path, &cli).unwrap();
                        if !handle_frame(&mut ffmpeg, frame, is_final_frame) {
                            while let Ok(EncodedFrame {
                                frame,
                                is_final_frame,
                            }) = receiver.recv()
                            {
                                if handle_frame(&mut ffmpeg, frame, is_final_frame) {
                                    break;
                                }
                            }
                        }
                        ffmpeg.encode_eof().unwrap();
                    }
                })
                .unwrap(),
        );
    }

    // The render channel connects the main thread (rendering) to the frame processor thread.
    let (render_sender, render_receiver) = mpsc::sync_channel::<RenderedFrame>(10);
    let cli_ = cli.clone();
    let conversion_thread = thread::Builder::new()
        .spawn(|| {
            let output_paths = output_paths;
            let cli = cli_;
            render_receiver.into_iter().par_bridge().for_each(|frame| {
                if !cli.images {
                    let mut processor = FrameProcessor::new(cli.resolution).unwrap();
                    let encode_frame = processor.process(&frame).unwrap();
                    encode_senders[frame.camera_index]
                        .send(encode_frame)
                        .unwrap();
                } else {
                    let image = frame.frame.download_mapped_rgba();
                    let index = frame.image_idx;
                    let extension = format!("{index:05}.png");
                    let output_path = output_paths[frame.camera_index].with_extension(extension);
                    image.save(output_path).unwrap();
                }
            });
            mem::drop(encode_senders);
        })
        .unwrap();
    /*
    .for_each_init(
        FrameProcessor::new(cli.resolution),
        |processor: &mut FrameProcessor, (frame, frame_index)| {
            let process
            let encode_frame = processor.process(frame, frame_index).unwrap();
            encode_sender.send((encode_frame, frame_index)).unwrap();
        },
    );
        */

    let frame_time = 1. / cli.fps as f64;
    let mut time = renderer.demo.current_time();
    let mut camera = Camera::new(cli.resolution.0 as f32 / cli.resolution.1 as f32);
    camera.zoom = Vec2::new(cli.zoom, cli.zoom);

    let mut tripods_frame_iter = tripods.as_ref().map(|tp| tp.iter_frames());
    let mut global_frame_i = 0;
    let mut single_frame_info: FrameInfo;

    for _ in 0.. {
        let frame_info = match &mut tripods_frame_iter {
            None => {
                let tick = time * TPS;
                time += frame_time; // Advance tick for next frame
                if cli.start_tick.map_or(false, |st| tick < st as f64) {
                    continue;
                } else if cli.end_tick.map_or(false, |et| tick > et as f64) {
                    break;
                }
                let frame_index = global_frame_i;
                global_frame_i += 1;
                if let Some(pos) = renderer.demo.camera_position() {
                    camera.position = pos;
                }
                single_frame_info = FrameInfo {
                    frame_index,
                    image_index: frame_index as usize,
                    camera_index: 0,
                    tick,
                    camera,
                    last_frame: false,
                };
                std::slice::from_ref(&single_frame_info)
            }
            Some(tripod_frames) => match tripod_frames.next() {
                None => break,
                Some(next) => next,
            },
        };
        match renderer.render_next_frames(frame_info)? {
            None => break, // Demo is over
            Some(frames) => {
                for (info, frame) in frames {
                    let frame = Arc::new(frame);
                    let send_frame = frame.clone();
                    let render_sender = render_sender.clone();
                    frame.map(
                        move || {
                            render_sender
                                .send(RenderedFrame {
                                    camera_index: info.camera_index,
                                    frame: send_frame,
                                    frame_idx: info.frame_index,
                                    image_idx: info.image_index,
                                    is_final_frame: info.last_frame,
                                })
                                .unwrap()
                        },
                        &renderer.device,
                        &renderer.queue,
                    )
                }
            }
        }
    }
    mem::drop(render_sender);

    renderer.device.poll(wgpu::Maintain::Wait);
    conversion_thread.join().unwrap();
    for encoder_thread in encoder_threads {
        encoder_thread.join().unwrap();
    }

    Ok(())
}

#[allow(unused)]
struct RenderIterator {
    width: u32,
    height: u32,
    pub demo: DemoProcessor,
    twmap: twmap::TwMap,
    instance: Instance,
    adapter: Adapter,
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,
    samplers: Arc<Samplers>,
    gpu_camera: Arc<GpuCamera>,
    map_static: GpuMapStatic,
    map_data_static: GpuMapDataStatic,
    map_data_dyn: GpuMapDataDyn,
    map_render: GpuMapRender,
    sprites_static: SpritesStatic,
    sprite_buffer: SpriteBuffer,
    texture_buffer: TextureBuffer,
    sprite_cache: SpriteCache,
    sprite_batcher: BatchSpriteRender,
    particles: Particles,
    textures: SpriteTextures,
    skin_manager: SkinManager,
    cli: Arc<Cli>,
}

impl RenderIterator {
    pub fn new(cli: Arc<Cli>) -> Result<Self, Box<dyn Error>> {
        println!("Connecting to GPU backend");
        let instance = Instance::new(&InstanceDescriptor {
            backends: Backends::all(),
            ..InstanceDescriptor::default()
        });
        let adapter =
            wgpu::util::initialize_adapter_from_env(&instance, None).unwrap_or_else(|| {
                instance
                    .request_adapter(&RequestAdapterOptions {
                        power_preference: PowerPreference::HighPerformance,
                        force_fallback_adapter: false,
                        compatible_surface: None,
                    })
                    .block_on()
                    .expect("No suitable adapter found")
            });
        eprintln!("{:#?}", adapter.get_info());
        eprintln!("Adapter {:#?}", adapter.limits());
        let get_device_desc = match cli.compatibility {
            true => device_descriptor,
            false => device_descriptor_for_batched_sprites,
        };
        let device_descriptor = get_device_desc(&adapter);
        let (device, queue) = adapter
            .request_device(&device_descriptor, None)
            .block_on()?;
        let device = Arc::new(device);
        let queue = Arc::new(queue);

        println!("Loading demo and map");
        let (demo, twmap, _map) = DemoProcessor::new_by_extension(&cli.demo, cli.map.as_deref())?;

        println!("Uploading data to the GPU");
        let (width, height) = cli.resolution;
        let samplers = Arc::new(Samplers::new(&device));
        let gpu_camera = Arc::new(GpuCamera::upload(&Camera::new(1.), &device));

        let map_static = GpuMapStatic::new(FORMAT, &device);
        let map_data_static = GpuMapDataStatic::upload(&twmap, &device, &queue);
        let map_data_dyn = GpuMapDataDyn::upload(&twmap, &device);
        let map_render = map_static.prepare_render(
            &twmap,
            &map_data_static,
            &map_data_dyn,
            &gpu_camera,
            &samplers,
            &device,
        );

        let sprites_static = SpritesStatic::new(FORMAT, &device);
        let sprite_buffer = device.vec_buffer(Some("Sprite Vertices"), wgpu::BufferUsages::VERTEX);
        let texture_buffer =
            device.vec_buffer(Some("Sprite Textures"), wgpu::BufferUsages::STORAGE);
        let sprite_batcher = BatchSpriteRender::new(FORMAT, &device);
        let mut sprite_cache = SpriteCache::default();
        if let Some(limit) = cli.max_sprites {
            sprite_cache.set_limit(limit)
        }
        let particles = Particles::new(demo.current_time());
        let mut textures = SpriteTextures::new(&device, &queue, &gpu_camera, &samplers);
        if !cli.compatibility {
            textures.turn_off_compatibility();
        }
        init_sprite_textures(&mut textures, twmap.version, &device, &queue)?;
        let blit = Arc::new(Blit::new(&device));
        let skin_manager = SkinManager::new(blit, &mut textures, device.clone(), queue.clone());

        Ok(Self {
            width,
            height,
            demo,
            twmap,
            instance,
            adapter,
            device,
            queue,
            samplers,
            gpu_camera,
            map_static,
            map_data_static,
            map_data_dyn,
            map_render,
            sprites_static,
            sprite_buffer,
            texture_buffer,
            sprite_cache,
            sprite_batcher,
            particles,
            textures,
            skin_manager,
            cli,
        })
    }

    /// Renders all camera views for the next time stamp.
    pub fn render_next_frames(
        &mut self,
        frame_info: &[FrameInfo],
    ) -> Result<Option<Vec<(FrameInfo, DownloadTexture)>>, Box<dyn Error>> {
        let tick = frame_info.first().unwrap().tick;
        if self.demo.to_tick(tick, self.cli.skip_threshold)?.is_none() {
            return Ok(None); // Demo is over
        }
        if let Some(new_snap) = self.demo.new_snap() {
            self.skin_manager.queue_snap_skins(new_snap);
            self.skin_manager.wait_for_queued(&mut self.textures);
        }

        let size = Vec2::new(self.width, self.height);
        let time_m = (tick / TPS * 1000.) as i64;

        let frames: Vec<(FrameInfo, DownloadTexture)> = frame_info
            .iter()
            .map(|info| {
                (
                    *info,
                    DownloadTexture::new(self.width, self.height, FORMAT, &self.device),
                )
            })
            .collect();

        self.sprite_cache.clear();
        let mut rng = Rng::new((tick * 1000.) as u32);
        let mut render_ctx = self
            .demo
            .render_ctx(&mut self.particles, &self.textures, &mut rng);
        let mut rng = Rng::new((tick * 1000.) as u32);
        render_ctx.rng = &mut rng;

        if self.cli.max_sprites.is_some() {
            for (info, frame) in &frames {
                let camera = &info.camera;
                self.map_data_dyn.update(
                    &self.twmap,
                    &self.map_data_static,
                    camera,
                    size,
                    time_m,
                    time_m,
                    &self.queue,
                );
                self.gpu_camera.update(camera, &self.queue);
                let mut command_encoder = new_command_encoder(&self.device);
                let frame = frame.texture_view();
                let render_pass = new_render_pass(
                    &mut command_encoder,
                    &frame,
                    LoadOp::Clear(Color {
                        r: 0.,
                        g: 0.,
                        b: 0.,
                        a: 1.,
                    }),
                );
                let mut tw_render_pass = TwRenderPass::new(render_pass, size, camera);
                self.map_render.render_background(&mut tw_render_pass);
                mem::drop(tw_render_pass);
                self.queue.submit([command_encoder.finish()]);
            }

            #[allow(clippy::too_many_arguments)]
            fn render_sprites_chunked(
                frames: &[(FrameInfo, DownloadTexture)],
                sprites_static: &SpritesStatic,
                sprite_buffer: &mut SpriteBuffer,
                texture_buffer: &mut TextureBuffer,
                sprite_cache: &mut SpriteCache,
                sprite_batcher: &mut BatchSpriteRender,
                compatibility: bool,
                gpu_camera: &GpuCamera,
                textures: &SpriteTextures,
                device: &Device,
                queue: &Queue,
            ) {
                let mut render_bundle = new_render_bundle(device);
                sprite_buffer
                    .upload(sprite_cache.sprites(), device, queue)
                    .expect("GPU Buffer size limited exceeded");
                if compatibility {
                    sprites_static.render(
                        sprite_buffer,
                        sprite_cache.textures(),
                        textures,
                        &mut render_bundle,
                    );
                } else {
                    texture_buffer
                        .upload(sprite_cache.textures(), device, queue)
                        .unwrap();
                    sprite_batcher.render(
                        sprite_buffer,
                        texture_buffer,
                        textures,
                        &mut render_bundle,
                        device,
                    );
                }
                let render_bundle = finish_bundle(render_bundle);

                for (info, frame) in frames {
                    let mut command_encoder = new_command_encoder(device);
                    let frame = frame.texture_view();
                    let mut render_pass =
                        new_render_pass(&mut command_encoder, &frame, LoadOp::Load);
                    gpu_camera.update(&info.camera, queue);
                    // TODO: Hier camera updaten
                    render_pass.execute_bundles([&render_bundle]);
                    mem::drop(render_pass);
                    queue.submit([command_encoder.finish()]);
                }
                sprite_cache.clear();
            }

            let mut particles = render_ctx.particles.get_group(ParticleGroup::Trails);
            let mut finished = false;
            while !finished {
                finished = self.sprite_cache.drain_sprites(&mut particles);
                render_sprites_chunked(
                    &frames,
                    &self.sprites_static,
                    &mut self.sprite_buffer,
                    &mut self.texture_buffer,
                    &mut self.sprite_cache,
                    &mut self.sprite_batcher,
                    self.cli.compatibility,
                    &self.gpu_camera,
                    &self.textures,
                    &self.device,
                    &self.queue,
                );
            }

            let mut snap_sprite_generator = render_ctx.snap_sprite_generator();
            let mut finished = false;
            while !finished {
                finished = self.sprite_cache.drain_sprites(&mut snap_sprite_generator);
                render_sprites_chunked(
                    &frames,
                    &self.sprites_static,
                    &mut self.sprite_buffer,
                    &mut self.texture_buffer,
                    &mut self.sprite_cache,
                    &mut self.sprite_batcher,
                    self.cli.compatibility,
                    &self.gpu_camera,
                    &self.textures,
                    &self.device,
                    &self.queue,
                );
            }

            for (info, frame) in &frames {
                let camera = &info.camera;
                self.map_data_dyn.update(
                    &self.twmap,
                    &self.map_data_static,
                    camera,
                    size,
                    time_m,
                    time_m,
                    &self.queue,
                );
                self.gpu_camera.update(camera, &self.queue);
                let mut command_encoder = new_command_encoder(&self.device);
                let frame = frame.texture_view();
                let render_pass = new_render_pass(&mut command_encoder, &frame, LoadOp::Load);
                let mut tw_render_pass = TwRenderPass::new(render_pass, size, camera);
                self.map_render.render_foreground(&mut tw_render_pass);
                mem::drop(tw_render_pass);
                self.queue.submit([command_encoder.finish()]);
            }

            for group in [
                ParticleGroup::Explosions,
                ParticleGroup::Extra,
                ParticleGroup::General,
            ] {
                let mut particles = self.particles.get_group(group);
                let mut finished = false;
                while !finished {
                    finished = self.sprite_cache.drain_sprites(&mut particles);
                    render_sprites_chunked(
                        &frames,
                        &self.sprites_static,
                        &mut self.sprite_buffer,
                        &mut self.texture_buffer,
                        &mut self.sprite_cache,
                        &mut self.sprite_batcher,
                        self.cli.compatibility,
                        &self.gpu_camera,
                        &self.textures,
                        &self.device,
                        &self.queue,
                    );
                }
            }
        } else {
            fn drain_sprites<T: SpriteGenerator>(
                cache: &mut SpriteCache,
                generator: &mut T,
            ) -> Range<usize> {
                let start = cache.len();
                assert!(cache.drain_sprites(generator));
                let end = cache.len();
                start..end
            }

            self.sprite_cache.clear();
            let snap_sprites = drain_sprites(
                &mut self.sprite_cache,
                &mut render_ctx.snap_sprite_generator(),
            );
            let particle_groups = ParticleGroup::ALL.map(|group| {
                drain_sprites(&mut self.sprite_cache, &mut self.particles.get_group(group))
            });
            self.sprite_buffer
                .upload(self.sprite_cache.sprites(), &self.device, &self.queue)
                .expect("GPU Buffer size limit exceeded");
            if !self.cli.compatibility {
                self.texture_buffer
                    .upload(self.sprite_cache.textures(), &self.device, &self.queue)
                    .unwrap();
            }

            for (info, frame) in &frames {
                let camera = &info.camera;
                self.map_data_dyn.update(
                    &self.twmap,
                    &self.map_data_static,
                    camera,
                    size,
                    time_m,
                    time_m,
                    &self.queue,
                );
                self.gpu_camera.update(camera, &self.queue);
                let mut command_encoder = new_command_encoder(&self.device);
                let frame = frame.texture_view();
                let render_pass = new_render_pass(
                    &mut command_encoder,
                    &frame,
                    LoadOp::Clear(Color {
                        r: 0.,
                        g: 0.,
                        b: 0.,
                        a: 1.,
                    }),
                );
                let mut tw_render_pass = TwRenderPass::new(render_pass, size, camera);
                self.map_render.render_background(&mut tw_render_pass);
                mem::drop(tw_render_pass);
                self.queue.submit([command_encoder.finish()]);
            }

            for (info, frame) in &frames {
                let mut sprites_encoder = new_command_encoder(&self.device);
                self.gpu_camera.update(&info.camera, &self.queue);
                let frame = frame.texture_view();
                let mut render = |range| {
                    let mut render_pass =
                        new_render_pass(&mut sprites_encoder, &frame, LoadOp::Load);
                    if self.cli.compatibility {
                        self.sprites_static.render_range(
                            range,
                            &self.sprite_buffer,
                            self.sprite_cache.textures(),
                            &self.textures,
                            &mut render_pass,
                        );
                    } else {
                        self.sprite_batcher.render_range(
                            range,
                            &self.sprite_buffer,
                            &self.texture_buffer,
                            &self.textures,
                            &mut render_pass,
                            &self.device,
                        )
                    }
                };
                render(particle_groups[ParticleGroup::Trails].clone());
                render(snap_sprites.clone());
                self.queue.submit([sprites_encoder.finish()]);
            }
            for (info, frame) in &frames {
                let camera = &info.camera;
                self.map_data_dyn.update(
                    &self.twmap,
                    &self.map_data_static,
                    camera,
                    size,
                    time_m,
                    time_m,
                    &self.queue,
                );
                self.gpu_camera.update(camera, &self.queue);
                let mut command_encoder = new_command_encoder(&self.device);
                let frame = frame.texture_view();
                let render_pass = new_render_pass(&mut command_encoder, &frame, LoadOp::Load);
                let mut tw_render_pass = TwRenderPass::new(render_pass, size, camera);
                self.map_render.render_foreground(&mut tw_render_pass);
                mem::drop(tw_render_pass);
                self.queue.submit([command_encoder.finish()]);
            }
            let mut sprites_encoder = new_command_encoder(&self.device);
            for (info, frame) in &frames {
                self.gpu_camera.update(&info.camera, &self.queue);
                let frame = frame.texture_view();
                let mut render = |ref range: Range<usize>| {
                    let mut render_pass =
                        new_render_pass(&mut sprites_encoder, &frame, LoadOp::Load);
                    if self.cli.compatibility {
                        self.sprites_static.render_range(
                            range.clone(),
                            &self.sprite_buffer,
                            self.sprite_cache.textures(),
                            &self.textures,
                            &mut render_pass,
                        );
                    } else {
                        self.sprite_batcher.render_range(
                            range.clone(),
                            &self.sprite_buffer,
                            &self.texture_buffer,
                            &self.textures,
                            &mut render_pass,
                            &self.device,
                        )
                    }
                };
                for group in [
                    ParticleGroup::Explosions,
                    ParticleGroup::Extra,
                    ParticleGroup::General,
                ] {
                    render(particle_groups[group].clone());
                }
            }
            self.queue.submit([sprites_encoder.finish()]);
        }

        Ok(Some(frames))
    }
}

fn new_command_encoder(device: &Device) -> CommandEncoder {
    device.create_command_encoder(&CommandEncoderDescriptor { label: LABEL })
}

fn new_render_bundle(device: &Device) -> RenderBundleEncoder {
    device.create_render_bundle_encoder(&RenderBundleEncoderDescriptor {
        label: LABEL,
        color_formats: &[Some(FORMAT)],
        depth_stencil: None,
        sample_count: 1,
        multiview: None,
    })
}

fn finish_bundle(encoder: RenderBundleEncoder) -> RenderBundle {
    encoder.finish(&RenderBundleDescriptor { label: LABEL })
}

fn new_render_pass<'a>(
    encoder: &'a mut CommandEncoder,
    view: &'a TextureView,
    load_op: LoadOp<Color>,
) -> RenderPass<'a> {
    encoder.begin_render_pass(&RenderPassDescriptor {
        label: LABEL,
        color_attachments: &[Some(RenderPassColorAttachment {
            view,
            resolve_target: None,
            ops: Operations {
                load: load_op,
                store: StoreOp::Store,
            },
        })],
        depth_stencil_attachment: None,
        timestamp_writes: None,
        occlusion_query_set: None,
    })
}

struct FfmpegEncoder {
    octx: format::context::Output,
    encoder: codec::encoder::video::Encoder,
    hw_ctx: Option<*mut AVBufferRef>,
    hw_frame: Option<ffmpeg::Frame>,
    packet: ffmpeg::Packet,
}

fn check(code: i32) -> Result<(), ffmpeg::Error> {
    match code {
        0 => Ok(()),
        _ => Err(ffmpeg::Error::from(code)),
    }
}
fn non_null<T>(ptr: *mut T) -> Result<*mut T, ffmpeg::Error> {
    if ptr.is_null() {
        Err(ffmpeg::Error::Unknown)
    } else {
        Ok(ptr)
    }
}

impl FfmpegEncoder {
    pub fn new(out_path: &Path, cli: &Cli) -> Result<Self, ffmpeg::Error> {
        ffmpeg::log::set_level(ffmpeg::log::Level::Info);
        let mut octx = format::output(out_path).unwrap();
        let (width, height) = cli.resolution;

        println!("Preparing encoder");

        let global_header = octx.format().flags().contains(format::Flags::GLOBAL_HEADER);
        let codec = match cli.vaapi {
            false => "libx264",
            true => "h264_vaapi",
        };
        let codec = encoder::find_by_name(codec).unwrap();
        let mut encoder = codec::context::Context::new_with_codec(codec)
            .encoder()
            .video()
            .unwrap();

        encoder.set_format(match cli.vaapi {
            false => CONVERT_FORMAT,
            true => HW_FORMAT,
        });
        encoder.set_width(width);
        encoder.set_height(height);
        let frame_rate = ffmpeg::Rational::new(cli.fps as i32, 1);
        encoder.set_frame_rate(Some(frame_rate));
        encoder.set_time_base(frame_rate.invert());
        if global_header {
            encoder.set_flags(codec::Flags::GLOBAL_HEADER);
        }

        let mut hw_frame = None;
        let mut hw_ctx = None;
        if cli.vaapi {
            unsafe {
                encoder.set_max_b_frames(0);
                let mut device_ctx: *mut AVBufferRef = ptr::null_mut();
                check(av_hwdevice_ctx_create(
                    &mut device_ctx,
                    AVHWDeviceType::AV_HWDEVICE_TYPE_VAAPI,
                    ptr::null(),
                    ptr::null_mut(),
                    0,
                ))
                .unwrap();
                let hw_buffer_ref = non_null(av_hwframe_ctx_alloc(device_ctx))?;
                let hw_frames_ctx = (*hw_buffer_ref).data as *mut AVHWFramesContext;
                (*hw_frames_ctx).format = HW_FORMAT.into();
                (*hw_frames_ctx).sw_format = CONVERT_FORMAT.into();
                (*hw_frames_ctx).width = width as i32;
                (*hw_frames_ctx).height = height as i32;
                check(av_hwframe_ctx_init(hw_buffer_ref)).unwrap();
                (*encoder.as_mut_ptr()).hw_frames_ctx = non_null(av_buffer_ref(hw_buffer_ref))?;
                let mut frame = ffmpeg_next::Frame::empty();
                check(av_hwframe_get_buffer(
                    (*encoder.as_mut_ptr()).hw_frames_ctx,
                    frame.as_mut_ptr(),
                    0,
                ))
                .unwrap();
                hw_ctx = Some(device_ctx);
                hw_frame = Some(frame);
            }
        }
        let mut options = ffmpeg::Dictionary::new();
        options.set("preset", cli.preset.to_possible_value().unwrap().get_name());
        options.set("x264-params", &cli.params);
        let encoder = encoder.open_with(options).unwrap();
        format::context::output::dump(&octx, 0, Some(out_path.as_os_str().to_str().unwrap()));

        let mut ost = octx.add_stream_with(&encoder).unwrap();
        ost.set_time_base(frame_rate.invert());
        octx.write_header().unwrap();
        let packet = ffmpeg::Packet::empty();

        Ok(Self {
            octx,
            encoder,
            hw_ctx,
            hw_frame,
            packet,
        })
    }

    fn process_packets(&mut self) -> Result<(), ffmpeg::Error> {
        while let Ok(()) = self.encoder.receive_packet(&mut self.packet) {
            self.packet.set_stream(0);
            self.packet.rescale_ts(
                self.encoder.time_base(),
                self.octx.stream(0).unwrap().time_base(),
            );
            self.packet.write(&mut self.octx).unwrap();
        }
        Ok(())
    }

    pub fn encode_frame(&mut self, frame: &util::frame::Video) -> Result<(), ffmpeg::Error> {
        if let Some(hw_frame) = &mut self.hw_frame {
            unsafe {
                check(av_hwframe_transfer_data(
                    hw_frame.as_mut_ptr(),
                    frame.as_ptr(),
                    0,
                ))
                .unwrap();
            }
            hw_frame.set_pts(frame.pts());
            self.encoder.send_frame(hw_frame)?;
        } else {
            self.encoder.send_frame(frame)?;
        }
        self.process_packets()?;
        Ok(())
    }

    pub fn encode_eof(&mut self) -> Result<(), ffmpeg::Error> {
        self.encoder.send_eof()?;
        self.process_packets()?;
        self.octx.write_trailer()?;
        if let Some(ctx) = &mut self.hw_ctx {
            unsafe {
                av_buffer_unref(ctx);
            }
        }
        Ok(())
    }
}

pub struct FrameProcessor {
    width: u32,
    height: u32,
    converter: ffmpeg::software::scaling::Context,
    render_frame: util::frame::Video,
}

impl FrameProcessor {
    pub fn new(resolution: (u32, u32)) -> Result<Self, ffmpeg::Error> {
        let (width, height) = resolution;
        let converter =
            ffmpeg::software::converter((width, height), RENDER_FORMAT, CONVERT_FORMAT)?;
        let render_frame = util::frame::Video::new(RENDER_FORMAT, width, height);
        Ok(Self {
            width,
            height,
            converter,
            render_frame,
        })
    }

    pub fn process(&mut self, frame: &RenderedFrame) -> Result<EncodedFrame, ffmpeg::Error> {
        let mut encode_frame = util::frame::Video::new(CONVERT_FORMAT, self.width, self.height);
        let mut rows = self
            .render_frame
            .data_mut(0)
            .chunks_exact_mut(self.width as usize * 3);
        frame.frame.download_mapped(|row| {
            let dest = rows.next().unwrap().chunks_exact_mut(3);
            row.chunks_exact(4)
                .map(|rgba| &rgba[..3])
                .zip(dest)
                .for_each(|(src, dest)| dest.copy_from_slice(src))
        });
        encode_frame.set_pts(Some(frame.frame_idx));
        self.converter
            .run(&self.render_frame, &mut encode_frame)
            .unwrap();
        Ok(EncodedFrame {
            frame: encode_frame,
            is_final_frame: frame.is_final_frame,
        })
    }
}
