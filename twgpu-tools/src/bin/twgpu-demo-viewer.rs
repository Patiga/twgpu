use clap::Parser;
use pollster::FutureExt;
use std::error::Error;
use std::mem;
use std::ops::Range;
use std::path::PathBuf;
use std::sync::Arc;
use std::time;
use std::time::Instant;
use twgpu::blit::Blit;
use twgpu::buffer::GpuDeviceExt;
use twgpu::shared::Rng;
use twgpu_tools::{init_sprite_textures, DemoProcessor, SkinManager};
use vek::Vec2;
use wgpu::{
    Backends, Color, CommandEncoderDescriptor, CompositeAlphaMode, Instance, InstanceDescriptor,
    LoadOp, Operations, PowerPreference, PresentMode, RenderPassColorAttachment,
    RenderPassDescriptor, RequestAdapterOptions, StoreOp, SurfaceConfiguration, TextureFormat,
    TextureUsages, TextureViewDescriptor,
};
use winit::dpi::PhysicalSize;
use winit::event::{DeviceEvent, Event, MouseScrollDelta, WindowEvent};
use winit::event_loop::ControlFlow;
use winit::{event_loop::EventLoop, window::WindowBuilder};

use twgpu::map::{GpuMapDataDyn, GpuMapDataStatic, GpuMapStatic};
use twgpu::sprites::{
    ParticleGroup, Particles, SpriteCache, SpriteGenerator, SpriteTextures, SpritesStatic,
};
use twgpu::textures::Samplers;
use twgpu::{device_descriptor, Camera, GpuCamera, TwRenderPass};

const FORMAT: TextureFormat = TextureFormat::Bgra8Unorm;
const LABEL: Option<&str> = Some("Map Inspect");

#[derive(Parser, Debug)]
#[clap(author, version, about = "View Teeworlds and DDNet maps")]
struct Cli {
    /// Path to the demo file
    demo: PathBuf,
    #[arg(long)]
    /// Provide or override the map
    map: Option<PathBuf>,
    /// Ticks per second
    #[arg(long, default_value = "50")]
    tps: u32,
    /// Enables v-sync. Relieves the GPU by limiting the fps
    #[arg(long)]
    vsync: bool,
    #[arg(long, default_value = "0")]
    start_tick: i32,
    /// Tick gap size where we start skipping.
    #[arg(long, default_value = "50")]
    skip_threshold: u32,
}

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let cli: Cli = Cli::parse();
    println!("Loading map");
    let (mut demo, twmap, _map) = DemoProcessor::new_by_extension(&cli.demo, cli.map.as_deref())?;
    if cli.start_tick != 0 {
        println!("Skipping to start tick");
        if demo
            .to_tick(cli.start_tick as f64, cli.skip_threshold)?
            .is_none()
        {
            println!("Demo already over, exiting");
            return Ok(());
        }
    }

    let instance = Instance::new(&InstanceDescriptor {
        backends: Backends::all(),
        ..InstanceDescriptor::default()
    });
    let adapter = wgpu::util::initialize_adapter_from_env(&instance, None).unwrap_or_else(|| {
        instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: None,
            })
            .block_on()
            .expect("No suitable adapter found")
    });
    eprintln!("{:#?}", adapter.get_info());
    eprintln!("Adapter {:#?}", adapter.limits());
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .block_on()?;
    let device = Arc::new(device);
    let queue = Arc::new(queue);

    println!("Uploading data to GPU");
    let samplers = Arc::new(Samplers::new(&device));
    let mut camera = Camera::new(1.);
    let gpu_camera = Arc::new(GpuCamera::upload(&camera, &device));

    let map_static = GpuMapStatic::new(FORMAT, &device);
    let map_data_static = GpuMapDataStatic::upload(&twmap, &device, &queue);
    let map_data_dyn = GpuMapDataDyn::upload(&twmap, &device);
    let map_render = map_static.prepare_render(
        &twmap,
        &map_data_static,
        &map_data_dyn,
        &gpu_camera,
        &samplers,
        &device,
    );

    let sprites_static = SpritesStatic::new(FORMAT, &device);
    let mut sprite_buffer = device.vec_buffer(Some("Sprite Vertices"), wgpu::BufferUsages::VERTEX);
    let mut sprite_cache = SpriteCache::default();
    let mut particles = Particles::new(demo.current_time());
    let mut textures = SpriteTextures::new(&device, &queue, &gpu_camera, &samplers);
    let mut rng = Rng::new(0);
    init_sprite_textures(&mut textures, twmap.version, &device, &queue)?;
    let blit = Arc::new(Blit::new(&device));
    let mut skin_manager = SkinManager::new(blit, &mut textures, device.clone(), queue.clone());

    println!("Opening window");
    let event_loop = EventLoop::new()?;
    let window = WindowBuilder::new().build(&event_loop)?;
    let window = Arc::new(window);
    let surface = instance.create_surface(window.clone())?;

    let PhysicalSize {
        mut width,
        mut height,
    } = window.inner_size();
    camera.switch_aspect_ratio(width as f32 / height as f32);
    let present_mode = match cli.vsync {
        false => PresentMode::AutoNoVsync,
        true => PresentMode::AutoVsync,
    };
    let mut config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format: FORMAT,
        width,
        height,
        present_mode,
        desired_maximum_frame_latency: 0,
        alpha_mode: CompositeAlphaMode::Auto,
        view_formats: Vec::new(),
    };
    surface.configure(&device, &config);

    let start = Instant::now();
    let mut fps = 0;
    let mut last_fps_stat = Instant::now();
    let start_tick = demo.start_tick();
    let start_tick_f = start_tick as f64;
    let ticks_per_micro = cli.tps as f64 / 1_000_000.;
    let mut last_tick = start_tick;

    event_loop.run(move |event, target| {
        target.set_control_flow(ControlFlow::Poll);

        match event {
            Event::DeviceEvent {
                event: device_event,
                ..
            } => match device_event {
                DeviceEvent::MouseMotion { delta } => {
                    camera.position += Vec2::<f64>::from(delta).az::<f32>() / 20.;
                }
                DeviceEvent::MouseWheel {
                    delta: MouseScrollDelta::LineDelta(_, dy),
                } => {
                    if dy.is_sign_positive() {
                        camera.zoom *= 1.1;
                    } else {
                        camera.zoom /= 1.1;
                    }
                }
                _ => {}
            },
            Event::WindowEvent {
                event: window_event,
                ..
            } => match window_event {
                WindowEvent::Resized(size) => {
                    PhysicalSize { width, height } = size;
                    config.width = width;
                    config.height = height;
                    surface.configure(&device, &config);
                    camera.switch_aspect_ratio(width as f32 / height as f32);
                }
                WindowEvent::CloseRequested => target.exit(),
                WindowEvent::RedrawRequested => {
                    if last_fps_stat.elapsed().as_secs() >= 5 {
                        last_fps_stat = time::Instant::now();
                        eprintln!("Fps: {}", fps / 5);
                        fps = 0;
                    }
                    fps += 1;
                    let time = start.elapsed().as_micros() as i64;
                    let size = Vec2::new(width, height);
                    map_data_dyn.update(
                        &twmap,
                        &map_data_static,
                        &camera,
                        size,
                        time,
                        time,
                        &queue,
                    );
                    gpu_camera.update(&camera, &queue);

                    let elapsed_f = start.elapsed().as_micros() as f64;
                    let tick_f = start_tick_f + elapsed_f * ticks_per_micro;
                    if demo.to_tick(tick_f, cli.skip_threshold).unwrap().is_none() {
                        target.exit();
                    }
                    if let Some(new_snap) = demo.new_snap() {
                        skin_manager.queue_snap_skins(new_snap);
                    }
                    if last_tick != tick_f as i32 {
                        skin_manager.poll_queued(&mut textures);
                        last_tick = tick_f as i32;
                    }
                    sprite_cache.clear();
                    let mut ctx = demo.render_ctx(&mut particles, &textures, &mut rng);

                    fn drain_sprites<T: SpriteGenerator>(
                        cache: &mut SpriteCache,
                        generator: &mut T,
                    ) -> Range<usize> {
                        let start = cache.len();
                        cache.drain_sprites(generator);
                        let end = cache.len();
                        start..end
                    }

                    let snap_sprites =
                        drain_sprites(&mut sprite_cache, &mut ctx.snap_sprite_generator());
                    let particle_groups = ParticleGroup::ALL.map(|group| {
                        drain_sprites(&mut sprite_cache, &mut particles.get_group(group))
                    });
                    sprite_buffer
                        .upload(sprite_cache.sprites(), &device, &queue)
                        .expect("GPU Buffer size limit exceeded");

                    if let Ok(frame) = surface.get_current_texture() {
                        let frame_view =
                            frame.texture.create_view(&TextureViewDescriptor::default());
                        let mut command_encoder = device
                            .create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                        let render_pass =
                            command_encoder.begin_render_pass(&RenderPassDescriptor {
                                label: LABEL,
                                color_attachments: &[Some(RenderPassColorAttachment {
                                    view: &frame_view,
                                    resolve_target: None,
                                    ops: Operations {
                                        load: LoadOp::Clear(Color {
                                            r: 0.0,
                                            g: 0.0,
                                            b: 0.0,
                                            a: 1.0,
                                        }),
                                        store: StoreOp::Store,
                                    },
                                })],
                                depth_stencil_attachment: None,
                                timestamp_writes: None,
                                occlusion_query_set: None,
                            });
                        let mut tw_render_pass = TwRenderPass::new(render_pass, size, &camera);
                        map_render.render_background(&mut tw_render_pass);
                        sprites_static.render_range(
                            particle_groups[ParticleGroup::Trails].clone(),
                            &sprite_buffer,
                            sprite_cache.textures(),
                            &textures,
                            &mut tw_render_pass.render_pass,
                        );
                        sprites_static.render_range(
                            snap_sprites,
                            &sprite_buffer,
                            sprite_cache.textures(),
                            &textures,
                            &mut tw_render_pass.render_pass,
                        );
                        map_render.render_foreground(&mut tw_render_pass);
                        for group in [
                            ParticleGroup::Explosions,
                            ParticleGroup::Extra,
                            ParticleGroup::General,
                        ] {
                            sprites_static.render_range(
                                particle_groups[group].clone(),
                                &sprite_buffer,
                                sprite_cache.textures(),
                                &textures,
                                &mut tw_render_pass.render_pass,
                            );
                        }
                        mem::drop(tw_render_pass);
                        queue.submit([command_encoder.finish()]);
                        frame.present();
                        window.request_redraw();
                    }
                }
                _ => {}
            },

            _ => (),
        }
    })?;
    Ok(())
}
