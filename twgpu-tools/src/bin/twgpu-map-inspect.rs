use clap::Parser;
use notify::Watcher;
use pollster::FutureExt;
use std::error::Error;
use std::path::PathBuf;
use std::sync::{self, Arc};
use std::{thread, time};
use twgpu_tools::gpu_map_from_path;
use vek::Vec2;
use wgpu::{
    Backends, Color, CommandEncoderDescriptor, CompositeAlphaMode, Instance, InstanceDescriptor,
    LoadOp, Operations, PowerPreference, PresentMode, RenderPassColorAttachment,
    RenderPassDescriptor, RequestAdapterOptions, StoreOp, SurfaceConfiguration, TextureFormat,
    TextureUsages, TextureViewDescriptor,
};
use winit::dpi::PhysicalSize;
use winit::event::{DeviceEvent, Event, MouseScrollDelta, WindowEvent};
use winit::event_loop::ControlFlow;
use winit::{event_loop::EventLoop, window::WindowBuilder};

use twgpu::map::GpuMapStatic;
use twgpu::textures::Samplers;
use twgpu::{device_descriptor, Camera, GpuCamera, TwRenderPass};

const FORMAT: TextureFormat = TextureFormat::Bgra8Unorm;
const LABEL: Option<&str> = Some("Map Inspect");

#[derive(Parser, Debug)]
#[clap(author, version, about = "View Teeworlds and DDNet maps")]
struct Cli {
    /// Path to the map
    map: PathBuf,
    /// Delay in seconds between each checks if the map has been modified
    #[arg(long, default_value = "0.1")]
    poll_interval: f32,
    /// Enables v-sync. Relieves the GPU by limiting the fps
    #[arg(long)]
    vsync: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let cli: Cli = Cli::parse();

    println!("Connecting to GPU backend");
    let instance = Instance::new(&InstanceDescriptor {
        backends: Backends::all(),
        ..InstanceDescriptor::default()
    });
    let adapter = wgpu::util::initialize_adapter_from_env(&instance, None).unwrap_or_else(|| {
        instance
            .request_adapter(&RequestAdapterOptions {
                power_preference: PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: None,
            })
            .block_on()
            .expect("No suitable adapter found")
    });
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .block_on()?;
    let device = Arc::new(device);
    let queue = Arc::new(queue);

    let mut camera = Camera::new(1.);
    let gpu_camera = Arc::new(GpuCamera::upload(&camera, &device));
    let samplers = Arc::new(Samplers::new(&device));
    let map_static = Arc::new(GpuMapStatic::new(FORMAT, &device));

    let (map_tx, map_rx) = sync::mpsc::channel();
    let (file_tx, file_rx) = sync::mpsc::channel();
    let notifier_cfg = notify::Config::default()
        .with_poll_interval(time::Duration::from_secs_f32(cli.poll_interval));
    let mut file_notifier = notify::PollWatcher::new(file_tx, notifier_cfg)?;
    let path = cli.map.clone();
    let m = map_static.clone();
    let c = gpu_camera.clone();
    let s = samplers.clone();
    let d = device.clone();
    let q = queue.clone();
    file_notifier.watch(&cli.map, notify::RecursiveMode::NonRecursive)?;
    thread::spawn(move || {
        while let Ok(event) = file_rx.recv() {
            let event = match event {
                Ok(event) => event,
                Err(err) => {
                    println!("Error on file notifier: {err}");
                    continue;
                }
            };
            println!("File event: {:?}", event.kind);
            println!("Paths: {:?}", event.paths);
            println!("Path: {:?}", path);
            match gpu_map_from_path(&path, &m, &c, &s, &d, &q) {
                Ok(data) => map_tx.send(data).unwrap(),
                Err(err) => {
                    println!("Error parsing updated map: {err}");
                    continue;
                }
            };
        }
    });

    let (mut map, map_data_static, mut map_data_dyn, mut map_render) = gpu_map_from_path(
        &cli.map,
        &map_static,
        &gpu_camera,
        &samplers,
        &device,
        &queue,
    )?;

    println!("Starting window");
    let event_loop = EventLoop::new()?;
    event_loop.set_control_flow(ControlFlow::Poll);
    let window = WindowBuilder::new().build(&event_loop)?;
    let window = Arc::new(window);

    let surface = instance.create_surface(window.clone())?;

    let PhysicalSize {
        mut width,
        mut height,
    } = window.inner_size();
    camera.switch_aspect_ratio(width as f32 / height as f32);
    let present_mode = match cli.vsync {
        false => PresentMode::AutoNoVsync,
        true => PresentMode::AutoVsync,
    };
    let mut config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format: FORMAT,
        width,
        height,
        present_mode,
        desired_maximum_frame_latency: 0,
        alpha_mode: CompositeAlphaMode::Auto,
        view_formats: Vec::new(),
    };
    surface.configure(&device, &config);

    let start = time::Instant::now();

    let mut fps = 0;
    let mut last_fps_stat = time::Instant::now();
    event_loop.run(move |event, target| {
        target.set_control_flow(ControlFlow::Poll);
        match event {
            Event::DeviceEvent {
                event: device_event,
                ..
            } => match device_event {
                DeviceEvent::MouseMotion { delta } => {
                    camera.position += Vec2::<f64>::from(delta).az::<f32>() / 20. * camera.zoom;
                }
                DeviceEvent::MouseWheel {
                    delta: MouseScrollDelta::LineDelta(_, dy),
                } => {
                    if dy.is_sign_positive() {
                        camera.zoom *= 1.1;
                    } else {
                        camera.zoom /= 1.1;
                    }
                }
                _ => {}
            },
            Event::WindowEvent {
                event: window_event,
                ..
            } => match window_event {
                WindowEvent::Resized(size) => {
                    PhysicalSize { width, height } = size;
                    config.width = width;
                    config.height = height;
                    surface.configure(&device, &config);
                    camera.switch_aspect_ratio(width as f32 / height as f32);
                }
                WindowEvent::CloseRequested => target.exit(),
                WindowEvent::RedrawRequested => {
                    if last_fps_stat.elapsed().as_secs() >= 5 {
                        last_fps_stat = time::Instant::now();
                        eprintln!("Fps: {}", fps as f32 / 5.);
                        fps = 0;
                    }
                    fps += 1;
                    let time = start.elapsed().as_micros() as i64;
                    let render_target_size = Vec2::new(width, height);
                    if let Ok(new_map) = map_rx.try_recv() {
                        map = new_map.0;
                        map_data_dyn = new_map.2;
                        map_render = new_map.3;
                    }
                    map_data_dyn.update(
                        &map,
                        &map_data_static,
                        &camera,
                        render_target_size,
                        time,
                        time,
                        &queue,
                    );
                    gpu_camera.update(&camera, &queue);
                    if let Ok(frame) = surface.get_current_texture() {
                        let frame_view =
                            frame.texture.create_view(&TextureViewDescriptor::default());
                        let mut command_encoder = device
                            .create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                        {
                            let render_pass =
                                command_encoder.begin_render_pass(&RenderPassDescriptor {
                                    label: LABEL,
                                    color_attachments: &[Some(RenderPassColorAttachment {
                                        view: &frame_view,
                                        resolve_target: None,
                                        ops: Operations {
                                            load: LoadOp::Clear(Color {
                                                r: 0.0,
                                                g: 0.0,
                                                b: 0.0,
                                                a: 1.0,
                                            }),
                                            store: StoreOp::Store,
                                        },
                                    })],
                                    depth_stencil_attachment: None,
                                    timestamp_writes: None,
                                    occlusion_query_set: None,
                                });
                            let mut tw_render_pass =
                                TwRenderPass::new(render_pass, render_target_size, &camera);
                            map_render.render_background(&mut tw_render_pass);
                            map_render.render_foreground(&mut tw_render_pass);
                        }
                        queue.submit([command_encoder.finish()]);
                        frame.present();
                        window.request_redraw();
                    }
                }
                _ => {}
            },

            _ => (),
        }
    })?;
    Ok(())
}
