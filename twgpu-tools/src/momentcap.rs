use arrayvec::{ArrayString, ArrayVec};
use bytemuck::{from_bytes, try_cast_slice};
use bytemuck_derive::{Pod, Zeroable};
use fixed::FixedI32;
use pack1::{I32LE, U64LE};
use std::mem;
use std::str;
use twsnap::{enums, flags, items, time, SkinColor, Snap, SnapId};
use vek::Vec2;

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct ClientInfo {
    name: [I32LE; 4],
    clan: [I32LE; 3],
    country: I32LE,
    skin: [I32LE; 6],
    use_custom_color: I32LE,
    color_body: I32LE,
    color_feet: I32LE,
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
struct CharacterCore {
    tick: I32LE,
    x: I32LE,
    y: I32LE,
    vel_x: I32LE,
    vel_y: I32LE,
    angle: I32LE,
    direction: I32LE,
    jumped: I32LE,
    hooked_player: I32LE,
    hook_state: I32LE,
    hook_tick: I32LE,
    hook_x: I32LE,
    hook_y: I32LE,
    hook_dx: I32LE,
    hook_dy: I32LE,
}

#[repr(C)]
#[derive(Clone, Copy, Pod, Zeroable)]
pub struct Pose {
    client_info: ClientInfo,
    core: CharacterCore,
    emote_type: I32LE,
    weapon: I32LE,
    timeout_code: [u8; 64],
    addr: [u8; 48],
}

impl Pose {
    fn into_player(&self, tick: time::Instant) -> items::Player {
        items::Player {
            name: ints_to_string(&self.client_info.name),
            clan: ints_to_string(&self.client_info.clan),
            country: self.client_info.country.get(),
            skin: ints_to_string(&self.client_info.skin),
            use_custom_color: self.client_info.use_custom_color.get() != 0,
            color_body: skin_color(self.client_info.color_body),
            color_feet: skin_color(self.client_info.color_feet),
            flags: flags::PlayerFlags::empty(),
            auth_level: enums::Authed::No,
            team: 0,
            local: false,
            teeworlds_team: enums::ClientTeam::Red,
            score: 0,
            latency: 0,
            tee: Some(items::Tee {
                tick,
                pos: Vec2 {
                    x: FixedI32::from_bits(self.core.x.get()),
                    y: FixedI32::from_bits(self.core.y.get()),
                },
                vel: Vec2 {
                    x: FixedI32::ZERO,
                    y: FixedI32::ZERO,
                },
                angle: FixedI32::from_bits(self.core.angle.get()),
                direction: enums::Direction::None,
                jumped: flags::JumpFlags::empty(),
                hooked_player: None,
                hook_state: if self.core.hook_state.get() > 0 {
                    enums::HookState::Grabbed
                } else {
                    enums::HookState::Idle
                },
                hook_tick: time::Duration::T0MS,
                hook_pos: Vec2 {
                    x: FixedI32::from_bits(self.core.hook_x.get()),
                    y: FixedI32::from_bits(self.core.hook_y.get()),
                },
                hook_direction: Vec2 {
                    x: FixedI32::ZERO,
                    y: FixedI32::ZERO,
                },
                flags: flags::TeeFlags::empty(),
                health: 10,
                armor: 0,
                ammo_count: 0,
                weapon: enums::ActiveWeapon::from(self.weapon.get()),
                emote: enums::Emote::from(self.emote_type.get()),
                attack_tick: time::Instant::zero(),
                freeze_end: time::Instant::zero(),
                jumps: 1,
                tele_checkpoint: 0,
                strong_weak_id: 0,
                jumped_total: 0,
                ninja_activation_tick: time::Instant::zero(),
                freeze_start: time::Instant::zero(),
                target: Vec2 {
                    x: FixedI32::ZERO,
                    y: FixedI32::ZERO,
                },
            }),
        }
    }
}

fn ints_to_string<const N1: usize, const N2: usize>(ints: &[I32LE; N1]) -> ArrayString<N2> {
    assert!(N1 * 4 == N2 + 1);
    let mut result: ArrayVec<u8, N2> = ArrayVec::new();
    let mut input = ints.iter().copied().map(I32LE::get);
    'outer: while let Some(int) = input.next() {
        for i in (0..4).rev() {
            if i == 0 && input.len() == 0 {
                // Skip the last "byte".
                break 'outer;
            }
            let v = ((int >> (i * 8)) as u8).wrapping_add(0x80);
            if v == 0 {
                // Break on null termination.
                break 'outer;
            }
            result.push(v);
        }
    }
    for i in (0..result.len() + 1).rev() {
        if let Ok(s) = str::from_utf8(&result[..i]) {
            return ArrayString::from(s).unwrap();
        }
    }
    unreachable!();
}

fn skin_color(c: I32LE) -> SkinColor {
    // TODO: Why the fuck is it `to_be_bytes`? It should be `to_le_bytes` like
    // in twsnap, src/compat/ddnet/from_ddnet.rs, `fn skin_color`.
    let [a, h, s, l_upper] = c.get().to_be_bytes();
    SkinColor { h, s, l_upper, a }
}

pub fn read_poses(bytes: &[u8]) -> Result<&[Pose], String> {
    if bytes.len() < mem::size_of::<U64LE>() {
        return Err("input bytes too short".into());
    }
    let (len, rest) = bytes.split_at(mem::size_of::<U64LE>());
    let len: &U64LE = from_bytes(len);
    assert!(mem::align_of::<Pose>() == 1);
    let invalid_poses_error = || {
        let num_extra_bytes = rest.len() % mem::size_of::<Pose>();
        let num_poses = (rest.len() / mem::size_of::<Pose>()) as u64;
        format!(
            "invalid poses file, expected {} poses, but found {} with {} extra bytes",
            len.get(),
            num_poses,
            num_extra_bytes,
        )
    };
    let result: &[Pose] = try_cast_slice(rest).map_err(|_| invalid_poses_error())?;
    if result.len() as u64 != len.get() {
        return Err(invalid_poses_error());
    }
    Ok(result)
}

pub fn to_snap(poses: &[Pose], snap: &mut Snap, tick: time::Instant) {
    snap.clear();
    for (i, pose) in poses.iter().enumerate() {
        snap.players
            .insert(SnapId(i.try_into().unwrap()), pose.into_player(tick));
    }
}
